// Sriramajayam

#include "StressWork.h"
#include "P12DElement.h"
#include "Assembler.h"
#include <cstdio>
#include <cassert>
#include <random>

int main(int argc, char **argv)
{
  PetscInitialize(&argc,&argv, PETSC_NULL, PETSC_NULL);

  // Reference configuration
  constexpr int SPD = 2;
  std::vector<double> Vertices({1,0,0,1,0,0,1,1,0,2});
  Triangle<SPD>::SetGlobalCoordinatesArray(Vertices);

  // Create 3 elements
  const int nElements = 3;
  std::vector< Element*> LocalElements(nElements);
  const int conn[] = {1,2,3,1,4,2,2,4,5};
  for(int e=0; e<nElements; e++)
    LocalElements[e] = new P12DElement<SPD>(conn[3*e],conn[3*e+1],conn[3*e+2]);

  // Local to global map
  StandardP12DMap L2GMap(LocalElements);
  
  // Create displacement map
  const int nNodes = static_cast<int>(Vertices.size())/SPD;
  VectorMap DispMap(nNodes, SPD);
  // Random nodal displacement
  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<> dis(-0.25,0.25);
  for(int a=0; a<nNodes; ++a)
    {
      double ndisp[] = {dis(gen), dis(gen)};
      DispMap.Set(a, ndisp);
    }
  DispMap.SetInitialized();

  // Create operations
  NeoHookean NH(1,1);
  Workspace WrkSpc(SPD);
  std::vector<StressWork*> LocalOperations(3);
  const std::vector<int> Fields({0,1});
  for(int e=0; e<nElements; e++)
    {
      VectorMapAccess VMAccess(std::vector<int>({conn[3*e]-1, conn[3*e+1]-1, conn[3*e+2]-1}));
      LocalOperations[e] = new StressWork(LocalElements[e], NH, Fields, VMAccess, WrkSpc);
    }

  // Create PETSc arrays
  PetscErrorCode ierr;
  const int dim = 10;
  assert(dim==L2GMap.GetTotalNumDof());
  Vec resVec;
  Mat dresMat;
  ierr = VecCreate(PETSC_COMM_WORLD, &resVec); CHKERRQ(ierr);
  ierr = VecSetSizes(resVec,PETSC_DECIDE,dim);  CHKERRQ(ierr);
  ierr = VecSetFromOptions(resVec); CHKERRQ(ierr);
  ierr = MatCreateSeqDense(PETSC_COMM_SELF,dim,dim,PETSC_NULL,&dresMat); CHKERRQ(ierr);
  ierr = MatSetOption(dresMat,MAT_SYMMETRIC,PETSC_TRUE); CHKERRQ(ierr);
  
  // Create assembler
  StandardAssembler<StressWork> Asm(LocalOperations, L2GMap);
  Asm.Assemble(&DispMap, resVec, dresMat);
  //ierr = VecView(resVec,PETSC_VIEWER_STDOUT_SELF); CHKERRQ(ierr);
  //ierr = MatView(dresMat,PETSC_VIEWER_STDOUT_SELF); CHKERRQ(ierr);

  Vec resVecPlus;
  Vec resVecMinus;
  ierr = VecDuplicate(resVec,&resVecPlus); CHKERRQ(ierr);
  ierr = VecDuplicate(resVec,&resVecMinus); CHKERRQ(ierr);
  
  Mat dresMatNum;
  ierr = MatDuplicate(dresMat, MAT_DO_NOT_COPY_VALUES, &dresMatNum); CHKERRQ(ierr);

  // Tolerances
  const double pertEPS = 1.e-6;
  const double tolEPS = 1.e-5;

  // Numerical derivatives
  for(int a=0; a<nNodes; ++a)
    for(int f=0; f<SPD; ++f)
      {
	// Residual at postive perturbation
	VectorMap pertDisp(DispMap);
	std::vector<double> pert(SPD);
	for(int i=0; i<SPD; ++i)
	  pert[i] = 0.;
	pert[f] = pertEPS;
	pertDisp.Increment(a, &pert[0]);
	pertDisp.SetInitialized();
	Asm.Assemble(&pertDisp, resVecPlus);

	// Residual at negative perturbation
	pert[f] = -2.*pertEPS;
	pertDisp.Increment(a, &pert[0]);
	pertDisp.SetInitialized();
	Asm.Assemble(&pertDisp, resVecMinus);

	// Central difference
	double *Plus, *Minus;
	ierr = VecGetArray(resVecPlus, &Plus); CHKERRQ(ierr);
	ierr = VecGetArray(resVecMinus, &Minus); CHKERRQ(ierr);

	for(int b=0; b<nNodes; ++b)
	  for(int g=0; g<SPD; ++g)
	    {
	      double deriv = (Plus[SPD*b+g]-Minus[SPD*b+g])/(2.*pertEPS);
	      ierr = MatSetValue(dresMatNum,SPD*a+f, SPD*b+g, deriv, INSERT_VALUES); CHKERRQ(ierr);
	    }
	ierr = VecRestoreArray(resVecPlus, &Plus); CHKERRQ(ierr); 
	ierr = VecRestoreArray(resVecMinus, &Minus); CHKERRQ(ierr);
      }
  
  ierr = MatAssemblyBegin(dresMatNum,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(dresMatNum,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr); 
  
  double *dresMatA;
  ierr = MatGetArray(dresMat,&dresMatA); CHKERRQ(ierr);
  double *dresMatNumA;
  ierr = MatGetArray(dresMatNum,&dresMatNumA); CHKERRQ(ierr);

  for(int i=0; i<dim*dim; i++)
    assert(std::abs(dresMatA[i]-dresMatNumA[i])<tolEPS);
      
  ierr = MatRestoreArray(dresMat,&dresMatA); CHKERRQ(ierr);
  ierr = MatRestoreArray(dresMatNum,&dresMatNumA); CHKERRQ(ierr);

  // Clean up
  for(int e=0; e<nElements; ++e)
    {
      delete LocalOperations[e];
      delete LocalElements[e];
    }
  ierr = VecDestroy(&resVecPlus); CHKERRQ(ierr);
  ierr = VecDestroy(&resVecMinus); CHKERRQ(ierr);
  ierr = MatDestroy(&dresMatNum); CHKERRQ(ierr);
  ierr = VecDestroy(&resVec); CHKERRQ(ierr);
  ierr = MatDestroy(&dresMat); CHKERRQ(ierr);
  PetscFinalize();
}
