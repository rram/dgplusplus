// Sriramajayam

#ifndef LOCAL_TO_GLOBAL_MAP
#define LOCAL_TO_GLOBAL_MAP

/**
   \brief LocalToGlobalMap class: map the local degrees of freedom
   of each Element to the global ones.
   
   The Local to Global map is strongly dependent on how the program
   that utilizes the Element objects is organized. The objective of
   this class is then to define the interface that the derived
   objects should have.
   
   There will generally not be a single LocalToGlobalMap object per
   Element, but rather only one LocalToGlobalMap for all of
   them. Hence, the interface requires a way to specify which
   element is being mapped.
   
   Convention:\n
   Fields and Dofs start to be numbered at 0
*/

class LocalToGlobalMap
{
 public:
  inline LocalToGlobalMap() {}

  inline virtual ~LocalToGlobalMap() {}

  inline LocalToGlobalMap(const LocalToGlobalMap &) {} 

  inline virtual LocalToGlobalMap* Clone() const = 0;
  
  //! @param field field number in the element, 0\f$  \le \f$ field \f$\le\f$ Nfields-1\n
  //! @param dof   number of degree of freedom in that field, \f$ 0 \le \f$ dof \f$\le\f$ Ndofs-1 \n
  //! @param elm: int of the Element whose degrees 
  //! of freedom are being mapped\n
  //! Map returns the GlobalDofIndex associated to degree of freedom "dof" 
  //! of field "field"
  //! in element MappedElement.
  virtual int Map(const int field, const int dof, const int elm) const = 0; 
  

  //! Total number of elements that can be mapped. Usually, total number of
  //! elements in the mesh.
  virtual int GetNumElements() const = 0;

  //! Number of fields in an element mapped
  virtual int GetNumFields(const int ElementMapped) const = 0;


  //! Number of dofs in an element mapped in a given field
  virtual int GetNumDofs(const int ElementMapped, const int field) const = 0;

  //! Total number of dof in the entire map
  virtual int GetTotalNumDof() const = 0; 
};


#endif
