/*
 * ShapesEvaluated.h
 * DG++
 *
 * Created by Adrian Lew on 9/7/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef SHAPESEVALUATED
#define SHAPESEVALUATED


#include "BasisFunctions.h"


/**
   ShapesEvaluated: Evaluation of the shape functions and derivatives at 
   the quadrature points. Abstract class.
   
   ShapesEvaluated is the class that takes the element geometry, a
   quadrature rule and shape functions on the reference element, and
   computes the values of the shape functions and their derivatives at
   the quadrature points. As opposed to Shapes, there will be one or
   more ShapesEvaluated objects per element in the mesh. Each different
   interpolation needed in an element will have a different
   ShapesEvaluated object.


   This class provides all functionality for the templated derived classes ShapesEvaluated__.
   Only two abstract functions are left to be defined by derived classes, 
   AccessShape and AccessQuadrature.

   Objects in this class should only be constructed by derived classes.

   ShapesEvaluated evaluates ElementGeometry::(D)Map at the quadrature
   point (Map)Coordinates using Quadrature::GetQuadraturePoint().

   ShapesEvaluated evaluates Shape functions at the quadrature
   point (Shape)Coordinates using Quadrature::GetQuadraturePointShape().

   \warning: The type of coordinates used to evaluate functions in
   Shape and ElementGeometry::(D)Map (barycentric, Cartesian, etc.)
   should be consistent with those provided in
   Quadrature::GetQuadraturePoint.  In other words, if the Quadrature
   object returns Cartesian coordinates, the Shape and
   ElementGeometry objects should evaluate functions taking
   Cartesian coordinates as arguments.

   \todo It would be nice to provide an interface of iterators to navigate
   the vectors, so that it is not necessary to remember in which order the
   data is stored in the vector. In the interest of simiplicity, this is 
   for the moment skipped.

   \todo It would useful to have the option of not computing the derivatives of the
   shape functions if not needed. Right now, it is not possible. 


   \todo Make coordinate types so that it is not necessary to check
   whether one is using the right convention between the three related
   classes.
   
   \todo The computation of derivatives of shape functions with
   respect to the coordinates of the embedding space can only be
   performed if the ElementGeometry::Map has domain and ranges of the
   same dimension. Otherwise, the derivatives should be computed with
   respect to some system of coordinates on the manifold. This will
   likely need to be revisited in the case of shells.

   \todo Need to fix the Lapack interface... it is too particular to
   Mac here, and even not the best way to do it in Mac...
*/




class ShapesEvaluated: public BasisFunctions
{
 public:
  //! \param QuadObj Quadrature object that defines integration rules on the element. 
  //! \param ShapeObj Shape object.
  //! \param EG Geometry of the element for which to evaluate shape functions.
  ShapesEvaluated(const Quadrature* QuadObj, const Shape* ShapeObj, 
		  const ElementGeometry* EG)
    {  CreateObject(QuadObj, ShapeObj, EG); }
  
  inline virtual ~ShapesEvaluated(){}
  
 ShapesEvaluated(const ShapesEvaluated &SE)
   :BasisFunctions(SE),
    LocalShapes(SE.LocalShapes), LocalDShapes(SE.LocalDShapes),
    LocalWeights(SE.LocalWeights), LocalCoordinates(SE.LocalCoordinates) 
    {}

  inline virtual ShapesEvaluated* Clone() const override
  { return new ShapesEvaluated(*this); }
  
  // Accessors/Mutators:
  inline const std::vector<double>& GetShapes() const override
  { return LocalShapes; }
  
  inline const std::vector<double>& GetDShapes() const override
  { return LocalDShapes; }
  
  inline const std::vector<double>& GetIntegrationWeights() const override
  { return LocalWeights;}
  
  inline const std::vector<double>& GetQuadraturePointCoordinates() const override
  { return LocalCoordinates; }
  
  //! returns the number of shape functions provided
  inline int GetBasisDimension() const override
  { return LocalShapes.size()/LocalWeights.size(); }
  
  //! returns the number of directional derivative for each shape function
  inline int GetNumberOfDerivativesPerFunction() const override
    { return LocalDShapes.size()/LocalShapes.size(); }
  
  //! returns the number of  number of coordinates for each Gauss point
  inline int GetSpatialDimensions() const override
  { return LocalCoordinates.size()/LocalWeights.size(); }
  
 private:
  //! Since it is not possible to have a virtual constructor, 
  //! one is emulated below only accessible from derived classes
  //! The virtual aspect are the calls to AccessShape and AccessQuadrature.
  void  CreateObject(const Quadrature* QuadObj, const Shape* ShapeObj, 
		     const ElementGeometry* EG);
  
  std::vector<double> LocalShapes;
  std::vector<double> LocalDShapes;
  std::vector<double> LocalWeights;
  std::vector<double> LocalCoordinates;
};

#endif
