// Sriramajayam

// Purpose : To check class Tetrahedron.

#include <iostream>
#include <cstdlib>
#include "Tetrahedron.h"
#include <cassert>

int main()
{
  std::vector<double> dummycoordinates({2,0,0, 
	0,2,0, 
	0,0,0, 
	0,0,2});
  Tetrahedron::SetGlobalCoordinatesArray(dummycoordinates);
  Tetrahedron MyTet(1,2,3,4);
  assert(MyTet.GetNumberOfVertices()==4);
  assert(MyTet.GetParametricDimension()==3); 
  assert(MyTet.GetEmbeddingDimension()==3);; 
  
  double X[3] = {0.25, 0.25, 0.25}; // Barycentric coordinates.
  assert(MyTet.ConsistencyTest(X,1.e-6));

  // Faces
  ElementGeometry *face = MyTet.GetFaceGeometry(1);
  assert(face->GetNumberOfVertices()==3);
  assert(face->GetParametricDimension()==2); 
  assert(face->GetEmbeddingDimension()==3); 
  auto& fconn =  face->GetConnectivity();
  assert(fconn[0]==3 && fconn[1]==1 && fconn[2]==4);
  delete face;

  // Test virtual mechanism and copy and clone constructors
  ElementGeometry *MyElmGeo = &MyTet;
  assert(MyElmGeo->GetPolytopeName()=="TETRAHEDRON");
  const auto  &Conn = MyElmGeo->GetConnectivity();
  assert(Conn[0]==1 && Conn[1]==2 && Conn[2]==3 && Conn[3]==4);
}

