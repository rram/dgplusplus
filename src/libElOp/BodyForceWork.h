/*
 * BodyForceWork.h
 * DG++
 *
 * Created by Adrian Lew on 09/12/07.
 *  
 * Copyright (c) 2007 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef BODYFORCEWORK
#define BODYFORCEWORK

#include <ElementalOperation.h>
#include <ForceField.h>
#include <StressWorkUtils.h>
#include <cassert>

/** 
    \brief Computes the virtual work of a body force, and its derivative

    The virtual work of a body force \f${\bf F}\f$ is defined as
    \f[
    \int_{E} F_i v_i\ d\Omega,
    \f]
    where \f${\bf v}\f$ is a virtual displacement field. Notice that
    the force field contains the same number of components as the
    virtual displacements \f$v_i\f$. This operation works for any
    number of components in the latter, in any number of spatial
    dimensions. The body force may depend on the unknown field
    (deformation mapping), and the position in the mesh (the position
    at the reference configuration).

    
    BodyForceWork needs a ForceField to compute the virtual work.

    BodyForceWork computes the residue
    \f[
    R[f][a] = \int_{E} F_{f} N_{a}\ d\Omega,
    \f]
    where \f$N_{a}\f$ is the shape function associated to degree of
    freedom \f$a\f$, \f$F_f\f$ is the component of the force field in
    direction \f$f\f$.

    The derivative of this residue is
    \f[
    DR[f][a][g][b] = \int_{E} DF_{fg} N_{a} N_{b} \ d\Omega,
    \f]
    where \f$DF\f$ is the gradient of the force field with respect to
    the unknown field (deformation mapping).
*/
class BodyForceWork: public DResidue
{
 public:
  //! Construct a BodyForceWork object with the field numbers provided. The order of these
  //! fields is also the order in which the scalar product with the body force components
  //! is performed.
  //! @param IElm pointer to the element over which the value will be computed. 
  //! The Input object is non-const, since these can be modified during the 
  //! operation.  The object pointed to is not destroyed when the operation is.
  //! @param FF ForceField object used. It is
  //! only referenced, not copied.
  //! @param fields Vector containing the field numbers in the Element to use. Field numbers
  //! begin from zero. 
  //! More specifically, for \f$v_i F_i\f$ we use the field number fields[i] for \f$v_i\f$. 
  //! The length of "fields" gives the number of fields used in the previous dot product.
 BodyForceWork(Element* IElm, const DForceField &FF,
	       const std::vector<int>& fields, VectorMapAccess access)
   :DResidue(), Elm(IElm), FieldsUsed(fields), LocalForceField(&FF), VMAccess(access)
    {
      assert((FF.GetForceLength()==FieldsUsed.size()) &&
	     "BodyForceWork: Incompatible sizes of the ForceField and the number of fields.");
    }
  
  inline virtual ~BodyForceWork() {}

  inline BodyForceWork(const BodyForceWork& BFW)
    :DResidue(BFW), Elm(BFW.Elm), FieldsUsed(BFW.FieldsUsed), 
    LocalForceField(BFW.LocalForceField), VMAccess(BFW.VMAccess) {}

  inline virtual BodyForceWork * Clone() const override
  { return new BodyForceWork(*this); }
  
  inline const std::vector<int>& GetField() const override
  { return FieldsUsed; }
  
  inline int GetFieldDof(int fieldnumber) const override
  { return Elm->GetDof(FieldsUsed[fieldnumber]); }
  
  
  //! \warning argval should contain displacements, not deformation mapping
  void GetDVal(const void* argval, 
	       std::vector<std::vector<double>>* funcval,
	       std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const override; 
  
  
  //! \warning argval should contain displacements, not deformation mapping
  inline void GetVal(const void* argval, 
		     std::vector<std::vector<double>>* funcval) const override 
  {  GetDVal(argval, funcval, 0); }
  
  //!  reference to the ForceField for the operation
  inline const ForceField&  GetForceField() const
  { return *LocalForceField; }

  //! Returns the element being used
  inline const Element* GetElement() const
  { return Elm; } 

  //! Consistency test
  //! \param[in] argval Configuration at which to perform the consistency test
  //! \param[in] pertEPS Tolerance to use for perturbation
  //! \param[in] pertEPS Tolerance to use for consistency check
  virtual bool ConsistencyTest(const void* config,
			       const double pertEPS,
			       const double tolEPS) const override;
  
 private:
  Element* Elm;
  std::vector<int> FieldsUsed;
  const DForceField* LocalForceField;
  const VectorMapAccess VMAccess;
};


#endif
