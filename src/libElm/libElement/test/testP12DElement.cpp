/*
 * testP12DElement.cpp
 * DG++
 *
 * Created by Adrian Lew on 9/22/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 


#include "P12DElement.h"
#include <iostream>
#include <cassert>
#include <cmath>

int main()
{ 
  std::vector<double> Vertices0({1,0,0,1,0,0});
  Triangle<2>::SetGlobalCoordinatesArray(Vertices0);
  P12DElement<2> TestElement(1,2,3);
  assert(TestElement.GetNumFields()==2);
  assert(TestElement.GetDof(0)==3);
  assert(TestElement.GetDof(1)==3);
  for(int a=0; a<2; a++)
    {
      std::cout << "Shape function values at quad points field("<< a<< "):\n";
      for(unsigned int q=0;q<TestElement.GetShape(a).size();q++)
	std::cout << TestElement.GetShape(a)[q] << " ";
      std::cout << "\n";
    }
 
  for(int a=0; a<2; a++)
    {
      std::cout << "Shape function derivatives values at quad points field("<< a<< "):\n";
      for(unsigned int q=0;q<TestElement.GetDShape(a).size();q++)
	std::cout << TestElement.GetDShape(a)[q] << " ";
      std::cout << "\n";
    }
 
  for(int a=0; a<2; a++)
    {
      std::cout << "Integration weight values at quad points field("<< a<< "):\n";
      for(unsigned int q=0;q<TestElement.GetIntegrationWeights(a).size();q++)
	std::cout << TestElement.GetIntegrationWeights(a)[q] << " ";
      std::cout << "\n";
    }

  for(int a=0; a<2; a++)
    {
      std::cout << "Quad points coordinates for field("<< a<< "):\n";
      for(unsigned int q=0;q<TestElement.GetIntegrationPointCoordinates(a).size();q++)
	std::cout << TestElement.GetIntegrationPointCoordinates(a)[q] << " ";
      std::cout << "\n";
    }

  P12DElement<2> CopyElement(TestElement);
  std::cout <<  "Test Copy Constructor\n";
    
  assert(CopyElement.GetNumFields()==2);
  assert(CopyElement.GetDof(0)==3);
  assert(CopyElement.GetDof(1)==3);
  for(int a=0; a<2; a++)
    {
      for(unsigned int q=0;q<CopyElement.GetShape(a).size();q++)
	assert(std::abs(CopyElement.GetShape(a)[q]-TestElement.GetShape(a)[q])<1.e-7);
	
      for(unsigned int q=0;q<CopyElement.GetDShape(a).size();q++)
	assert(std::abs(CopyElement.GetDShape(a)[q]-TestElement.GetDShape(q)[q])<1.e-7);
	
      for(unsigned int q=0;q<CopyElement.GetIntegrationWeights(a).size();q++)
	assert(std::abs(CopyElement.GetIntegrationWeights(a)[q]-TestElement.GetIntegrationWeights(a)[q])<1.e-7);

      for(unsigned int q=0;q<CopyElement.GetIntegrationPointCoordinates(a).size();q++)
	assert(std::abs(CopyElement.GetIntegrationPointCoordinates(a)[q]-TestElement.GetIntegrationPointCoordinates(a)[q])<1.e-7);
    }
}

