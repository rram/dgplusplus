/*
 * Subdivision.cpp
 * DG++
 *
 * Created by Adrian Lew and Ramsharan Rangarajan .
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 


#include "External.h"
#include <algorithm>

namespace{
  // temporary class for implementing subdivision
  class facestruct
  {
  public:
    int element;
    int eKey;
    int face;
    std::vector<int> conn;
  
    facestruct(int elem, int elemKey, int f, const std::vector<int> &co):
      element(elem), face(f),conn(co) 
    { 
      eKey = elemKey;
      sort(conn.begin(), conn.end()); 
    }
  
    facestruct(const facestruct &newfacest): 
      element(newfacest.element),face(newfacest.face),conn(newfacest.conn) 
    { eKey = newfacest.eKey; }
  
    bool operator<(const facestruct &other) const
    { return conn<other.conn; }
  };
}


void SubdivideTriangles(std::vector<int> & Conn, std::vector<double> &Coord, 
			int & nodes, int & elements)
{
  // Check for consistency of connectivity and coordinates arrays:
  std::vector<facestruct> Faces;
  
  unsigned int iElements = Conn.size()/3;  // 3 nodes per element.
  unsigned int iNodes = Coord.size()/2;    // Assume 2D triangles.
  
  for(unsigned int e=0; e<iElements; e++)
    {
      std::vector<int> conn;

      conn.push_back(Conn[e*3+0]);
      conn.push_back(Conn[e*3+1]);
      Faces.push_back(facestruct(e,e,0,conn));

      conn.clear();
      conn.push_back(Conn[e*3+1]);
      conn.push_back(Conn[e*3+2]);
      Faces.push_back(facestruct(e,e,1,conn));

      conn.clear();
      conn.push_back(Conn[e*3+2]);
      conn.push_back(Conn[e*3+0]);
      Faces.push_back(facestruct(e,e,2,conn));
    }	
  
  sort(Faces.begin(), Faces.end());

  std::vector<int>  NodeInfo(Faces.size()*2,0);
  int  middlenodenum = iNodes;

  // Create middle nodes
  for(std::vector<facestruct>::iterator it=Faces.begin(); it!=Faces.end(); it++)
    {
      double xm = (Coord[2*(it->conn[0]-1)] + Coord[2*(it->conn[1]-1)])/2.;
      double ym = (Coord[2*(it->conn[0]-1)+1] + Coord[2*(it->conn[1]-1)+1])/2.;
      Coord.push_back(xm);
      Coord.push_back(ym);
      
      NodeInfo[it->element*6+it->face*2+0] = it->face;
      NodeInfo[it->element*6+it->face*2+1] = middlenodenum+1;
      
      if(it+1!=Faces.end())
	if(it->conn[0] == (it+1)->conn[0] && it->conn[1] == (it+1)->conn[1])
	  {
	    it++;
	    NodeInfo[it->element*6+it->face*2+0] = it->face;
	    NodeInfo[it->element*6+it->face*2+1] = middlenodenum+1;
	  }
      
      middlenodenum++;
    }
  
  // Create connectivity
  std::vector<int> copyconn(Conn);
  Conn.resize(iElements*3*4);

  for(unsigned int e=0; e<iElements; e++)
    {
      // triangle 1
      Conn[e*4*3+ 0*3 + 0] = copyconn[e*3];
      Conn[e*4*3+ 0*3 + 1] = NodeInfo[e*6 + 0*2 + 1];
      Conn[e*4*3+ 0*3 + 2] = NodeInfo[e*6 + 2*2 + 1];

      // triangle 2
      Conn[e*4*3+ 1*3 + 0] = copyconn[e*3+1];
      Conn[e*4*3+ 1*3 + 1] = NodeInfo[e*6 + 1*2 + 1];
      Conn[e*4*3+ 1*3 + 2] = NodeInfo[e*6 + 0*2 + 1];

      // triangle 3
      Conn[e*4*3+ 2*3 + 0] = copyconn[e*3+2];
      Conn[e*4*3+ 2*3 + 1] = NodeInfo[e*6 + 2*2 + 1];
      Conn[e*4*3+ 2*3 + 2] = NodeInfo[e*6 + 1*2 + 1];

      // triangle 4
      Conn[e*4*3+ 3*3 + 0] = NodeInfo[e*6 + 0*2 + 1];
      Conn[e*4*3+ 3*3 + 1] = NodeInfo[e*6 + 1*2 + 1];
      Conn[e*4*3+ 3*3 + 2] = NodeInfo[e*6 + 2*2 + 1];
    }
  nodes = int(Coord.size()/2);
  elements = int(Conn.size()/3);
}



struct edgestruct
{
  int elem;
  int edge;
  std::vector<int> conn;
  
  edgestruct(int ielem, int iedge, std::vector<int> &iconn) 
    : elem(ielem), edge(iedge), conn(iconn)
  {
    std::sort(conn.begin(), conn.end());
    
  }

  edgestruct(const edgestruct &newedge)
    : elem(newedge.elem), edge(newedge.edge), conn(newedge.conn)
  {}
  
  bool operator<(const edgestruct &other) const
  { return (conn<other.conn); }

};




void GetEdgeNeighborList(std::vector<int> Conn,
			 std::vector< std::vector< std::vector<int> > > &Neighbors)
{

  unsigned int iElements = Conn.size()/4;
  int nEdges = 6; // 6 edges for a  tet.
  
  Neighbors.clear();
  Neighbors.resize(iElements);
  
  std::vector<edgestruct> EdgeStruct;  
  
  int V1[] = {0,1,0,2,0,1};
  int V2[] = {1,2,2,3,3,3};
  
  // Creating a list of all possible edges.
  for(unsigned int e=0; e<iElements; e++)
    {
      Neighbors[e].resize(nEdges); 
      
      int econn[] = {Conn[e*4+0],Conn[e*4+1],Conn[e*4+2],Conn[e*4+3]};
      
      for(int edgenum = 0; edgenum<nEdges; edgenum++)
	{
	  std::vector<int> edgeconn(2);
	  edgeconn[0] = econn[V1[edgenum]];
	  edgeconn[1] = econn[V2[edgenum]];
	  edgestruct *myedge = new edgestruct(e, edgenum, edgeconn);
	  EdgeStruct.push_back(*myedge);
	  delete myedge;
	}
    }
  std::sort(EdgeStruct.begin(), EdgeStruct.end());
  // Edges that have exactly the same connectivity should appear consecutively.
  // If there is no repetition, the edge is free.
  std::vector<edgestruct>::iterator it = EdgeStruct.begin();
  while(it != EdgeStruct.end())
    {
      std::vector<edgestruct> repedges;
      repedges.clear();
      repedges.push_back(*it);
      std::vector<edgestruct>::iterator it2 = it+1;
      while(true && it2!=EdgeStruct.end())
	{
	  if(it2->conn == it->conn)
	    {
	      repedges.push_back(*it2);
	      it2++;
	    }
	  else
	    break;
	}
      if(repedges.size() > 1) // Shared edge.
	{
	  for(unsigned int p=0; p<repedges.size(); p++)
	    {
	      for(unsigned int q=0; q<repedges.size(); q++)
		{
		  if(p!=q)
		    {
		      Neighbors[repedges[p].elem][repedges[p].edge].
			push_back(repedges[q].elem);
		      
		      Neighbors[repedges[p].elem][repedges[p].edge].
			push_back(repedges[q].edge);
		    }
		}
	    }
	}
      
      it = it2;
    }
  // done. 
}


/* Function: SubdivideTet
 * Purpose : Subdivide a tetrahedron in 8 smaller ones.
 * Algorithm to subdivide a test:
 * Parent tet: ABCD.
 * Since a consistent numbering of edges is crucial, the following convention 
 * is adopted : 1 - AB, 2-BC, 3-CA, 4-CD, 5-AD, 6-BD.
 * Midpoints of edges AB,BC,CA,CD,AD,BD are M1, M2, M3, M4, M5, M6 resply.

 * Tet1: A-M1-M3-M5,
 * Tet2: M1-B-M2-M6,
 * Tet3: M2-M2-C-M4,
 * Tet4: M5-M6-M4-D,

 * Tet5: M1-M4-M5-M6,
 * Tet6: M1-M4-M6-M2,
 * Tet7: M1-M4-M2-M3,
 * Tet8: M1-M4-M3-M5.
 */

void SubdivideTetrahedra(std::vector<int> & Conn, std::vector<double> & Coord,
			 int & nodes, int & elements)
{
  
  int sd = 3;
  int eNodes = 4;                       // Number of nodes per element.
  int nEdges = 6;                       // Tet has 6 edges.
  unsigned int iElements = Conn.size()/eNodes; // Number of elements.
  unsigned int iNodes = Coord.size()/sd;  
  
  std::vector< std::vector< std::vector<int> > > Neighbors;
  GetEdgeNeighborList(Conn,Neighbors);
  
  // Connectivity for mid-points of each edge for each element.
  int midconn[iElements][nEdges];
  int count = iNodes;
  
  for(unsigned int e=0; e<iElements; e++)
    {
      for(int f = 0; f<nEdges; f++)
	{
	  // Number of elements sharing edge 'f' of element 'e'.
	  
	  int nNeighbors = Neighbors[e][f].size()/2;
	  
	  if(nNeighbors == 0)  // Free edge.
	    {
	      count++;
	      midconn[e][f] = count;
	    }
	  
	  else // Shared edge
	    {
	      // Find the least element neighbor number.
	      int minElem = e;
	      for(int p=0; p<nNeighbors; p++)
		if(minElem > Neighbors[e][f][2*p])
		  minElem = Neighbors[e][f][2*p];
	      
	      if(int(e) == minElem) // Allot only once for a shared edge.
		{
		  count++;
		  midconn[e][f] = count;
		  
		  for(int p=0; p<nNeighbors; p++)
		    {
		      int nelem = Neighbors[e][f][2*p];
		      int nedge = Neighbors[e][f][2*p+1];
		      midconn[nelem][nedge] = count;
		    }
		}
	    }
	}
    }
  
  // Creating new coordinates and connectivity arrays:
  // Each tet is subdivided into 8.
  std::vector<double> NewCoord(count*sd);
  std::vector<int> NewConn;

  for(unsigned int i=0; i<Coord.size(); i++)
    NewCoord[i] = Coord[i];
  
  // Coordinates for midside nodes:
  int V1[] = {0,1,0,2,0,1};
  int V2[] = {1,2,2,3,3,3};
  for(unsigned int e=0; e<iElements; e++)
    for(int f=0; f<nEdges; f++)
      {
	int v1 = Conn[e*eNodes+V1[f]]-1;
	int v2 = Conn[e*eNodes+V2[f]]-1;
	for(int k=0; k<sd; k++)
	  NewCoord[(midconn[e][f]-1)*sd+k] = 0.5*(Coord[v1*sd+k]+Coord[v2*sd+k]);
      }
  
  for(unsigned int e=0; e<iElements; e++)
    {
      // tet 1
      int t1conn[] = {Conn[e*eNodes+0],
		      midconn[e][0],
		      midconn[e][2],
		      midconn[e][4]};
      for(int i=0; i<eNodes; i++)
	NewConn.push_back(t1conn[i]);
      
      // tet 2
      int t2conn[] = {midconn[e][0],
		      Conn[e*eNodes+1],
		      midconn[e][1],
		      midconn[e][5]};
      for(int i=0; i<eNodes; i++)
	NewConn.push_back(t2conn[i]);
      
      
      // tet3
      int t3conn[] = {midconn[e][2],
		      midconn[e][1],
		      Conn[e*eNodes+2],
		      midconn[e][3]};
      for(int i=0; i<eNodes; i++)
	NewConn.push_back(t3conn[i]);
      
      // tet4 
      int t4conn[] = {midconn[e][4],
		      midconn[e][5],
		      midconn[e][3],
		      Conn[e*eNodes+3]};
     
      for(int i=0; i<eNodes; i++)
	NewConn.push_back(t4conn[i]);
      
      // tet5 
      int t5conn[] = {midconn[e][0],
		      midconn[e][3],
		      midconn[e][4],
		      midconn[e][5]};
      for(int i=0; i<eNodes; i++)
	NewConn.push_back(t5conn[i]);
      
      // tet6
      int t6conn[] = {midconn[e][0],
		      midconn[e][3],
		      midconn[e][5],
		      midconn[e][1]};
      for(int i=0; i<eNodes; i++)
	NewConn.push_back(t6conn[i]);
      
      // tet 7
      int t7conn[] = {midconn[e][0],
		      midconn[e][3],
		      midconn[e][1],
		      midconn[e][2]};
      for(int i=0; i<eNodes; i++)
	NewConn.push_back(t7conn[i]);
      
      // tet 8
      int t8conn[] = {midconn[e][0],
		      midconn[e][3],
		      midconn[e][2],
		      midconn[e][4]};
      for(int i=0; i<eNodes; i++)
	NewConn.push_back(t8conn[i]);
    }
  Coord.clear();
  Conn.clear();
  Coord.assign(NewCoord.begin(), NewCoord.end());
  Conn.assign(NewConn.begin(), NewConn.end());
  nodes = int(Coord.size()/3);
  elements = int(Conn.size()/4);
}      
      



