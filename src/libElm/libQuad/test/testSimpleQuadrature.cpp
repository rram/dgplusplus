/*
 * testSimpleQuadrature.cpp
 * DG++
 *
 * Created by Adrian Lew on 9/7/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "Quadrature.h"
#include <iostream>
#include <cassert>
#include <cmath>

int main()
{
  // test class
  assert(Triangle_1::Bulk->GetNumberQuadraturePoints()==3);
  assert(Triangle_1::Bulk->GetNumberOfShapeCoordinates()==2);
  assert(Triangle_1::Bulk->GetNumberOfCoordinates()==2);
  
  const double qpts[] = {0.666667, 0.166667, 0.166667, 0.666667, 0.166667, 0.166667};
  const double qshp[] = {0.666667, 0.166667, 0.166667, 0.666667, 0.166667, 0.166667};
  const double qwts[] = {0.166667, 0.166667, 0.166667};
  
  for(int q=0; q<Triangle_1::Bulk->GetNumberQuadraturePoints(); q++)
    {
      for(int i=0; i<Triangle_1::Bulk->GetNumberOfCoordinates(); i++)
      assert(std::abs(Triangle_1::Bulk->GetQuadraturePoint(q)[i]-qpts[2*q+i])<1.e-6);
      
      for(int i=0; i<Triangle_1::Bulk->GetNumberOfShapeCoordinates(); i++)
	assert(std::abs(Triangle_1::Bulk->GetQuadraturePointShape(q)[i]-qshp[2*q+i])<1.e-6);
      
      assert(std::abs(Triangle_1::Bulk->GetQuadratureWeights(q)-qwts[q])<1.e-6);
    }

  // Test copy
  Quadrature GaussCopy(*Triangle_1::Bulk);
  assert(GaussCopy.GetNumberQuadraturePoints()==3);
  assert(GaussCopy.GetNumberOfShapeCoordinates()==2);
  assert(GaussCopy.GetNumberOfCoordinates()==2);
  for(int q=0; q<GaussCopy.GetNumberQuadraturePoints(); q++)
    {
      for(int i=0; i<GaussCopy.GetNumberOfCoordinates(); i++)
	assert(std::abs(GaussCopy.GetQuadraturePoint(q)[i]-qpts[2*q+i])<1.e-6);

      for(int i=0; i<GaussCopy.GetNumberOfShapeCoordinates(); i++)
	assert(std::abs(GaussCopy.GetQuadraturePointShape(q)[i]-qshp[2*q+i])<1.e-6);
      
      assert(std::abs(GaussCopy.GetQuadratureWeights(q)-qwts[q])<1.e-6);
    }

  // Test virtual mechanisms
  assert(Triangle_1::FaceOne->GetNumberQuadraturePoints()==2);
  assert(Triangle_1::FaceOne->GetNumberOfCoordinates()==1);
  assert(Triangle_1::FaceOne->GetNumberOfShapeCoordinates()==2);
  const double faceqpts[] = {0.788675, 0.211325};
  const double faceshpqpts[] = {0.788675, 0.211325, 0.211325, 0.788675};
  const double faceqwts[] = {0.5,0.5};
  for(int q=0; q<Triangle_1::FaceOne->GetNumberQuadraturePoints(); q++)
    {
      for(int i=0; i<Triangle_1::FaceOne->GetNumberOfCoordinates(); i++)
	assert(std::abs(Triangle_1::FaceOne->GetQuadraturePoint(q)[i]-faceqpts[q])<1.e-6);
      
      for(int i=0; i<Triangle_1::FaceOne->GetNumberOfShapeCoordinates(); i++)
	assert(std::abs(Triangle_1::FaceOne->GetQuadraturePointShape(q)[i]-faceshpqpts[2*q+i])<1.e-6);
      
      assert(std::abs(Triangle_1::FaceOne->GetQuadratureWeights(q)-faceqwts[q])<1.e-6);
    }

  // test copy constructor over face quadrature
  Quadrature NewTriangleFace(*Triangle_1::FaceOne);
  assert(NewTriangleFace.GetNumberQuadraturePoints()==2);
  assert(NewTriangleFace.GetNumberOfCoordinates()==1);
  assert(NewTriangleFace.GetNumberOfShapeCoordinates()==2);
  for(int q=0; q<NewTriangleFace.GetNumberQuadraturePoints(); q++)
    {
      for(int i=0; i<NewTriangleFace.GetNumberOfCoordinates(); i++)
	assert(std::abs(NewTriangleFace.GetQuadraturePoint(q)[i]-faceqpts[q])<1.e-6);
      
      for(int i=0; i<NewTriangleFace.GetNumberOfShapeCoordinates(); i++)
	assert(std::abs(NewTriangleFace.GetQuadraturePointShape(q)[i]-faceshpqpts[2*q+i])<1.e-6);
      
      assert(std::abs(NewTriangleFace.GetQuadratureWeights(q)-faceqwts[q])<1.e-6);
    }
}
