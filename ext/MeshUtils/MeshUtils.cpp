// Sriramajayam

/*
 * MeshUtils.cpp
 * DG++
 *
 * Created by Ramsharan Rangarajan on 08/20/2010.
 *  
 * Copyright (c) 2006 Adrian Lew.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "MeshUtils.h"
#include <algorithm>

// Position cursor at a word in a given file
bool PositionCursor(std::ifstream & ifile, const std::string & word)
{
  ifile.seekg(0, std::ios_base::beg);

  // positions cursor to get the data after 'word' occurs:
  while(ifile.good())
    {
      if(ifile.get() == word[0])
	{
	  unsigned int ipos=1;
	  while(ipos!= word.length())
	    if(ifile.get() == word[ipos]) ipos++;
	    else  break;
	  
	  if(ipos == word.length())
	    return true;
	}
    }
  return false;
}



std::vector<double> ReadTecplotFile(const std::string filename, CoordConn & MD, 
				    int NFields )
{
  // Open the given file
  std::ifstream TecFile;
  TecFile.open(filename, std::ifstream::in);
  assert(TecFile.good());
  if(TecFile.bad())
    {
      std::cerr<<"\nMeshUtils.cpp-ReadTecplotFile() - "
	       <<"Could not open requested file "<<filename<<"\n";
      std::fflush( stdout ); exit(1); }
  
  // Read  number of nodes:
  if(PositionCursor(TecFile, std::string("N=")))
    TecFile >> MD.nodes;
  else
    {
      std::cerr<<"\nMeshUtils.cpp-ReadTecplotFile()- Could not read number of nodes. ";
      exit(1);
    }
  
  // Read number of elements:
  if(PositionCursor(TecFile, std::string("E=")))
    TecFile>>MD.elements;
  else
    {
      std::cerr<<"\nMeshUtils.cpp-ReadTecplotFile()- Could not read number of elements.";
      exit(1);
    }
  
  // Read Element type:
  std::string ET;
  if(PositionCursor(TecFile, std::string("ET=")))
    TecFile>>ET;
  else
    {
      std::cerr<<"\nMeshUtils.cpp-ReadTecplotFile()- Could not read element type. Aborting.\n";
      exit(1);
    }
  
  if(ET == "TRIANGLE")
    MD.nodes_element = 3;
  else if(ET == "TETRAHEDRON")
    MD.nodes_element = 4;
  else if(ET == "QUADRILATERAL")
    MD.nodes_element = 4;
  else
    {
      std::cerr<<"\nMeshUtils.cpp-ReadTecplotFile():- Unknown element type. Aborting.\n";
      exit(1);
    }
  
  MD.coordinates.resize(MD.spatial_dimension*MD.nodes);
  MD.connectivity.resize(MD.nodes_element*MD.elements);
  
  fflush (stdout );

  // Reading coordinates along with dofs followed by the connectivity
  std::vector<double> Dofs;
  if(NFields != 0)
    {
      Dofs.resize(MD.nodes*NFields);
      
      if(PositionCursor(TecFile, ET))
	{
	  for(int a=0; a<MD.nodes; a++)
	    {
	      for(int k=0; k < MD.spatial_dimension; k++)
		TecFile>>MD.coordinates[MD.spatial_dimension*a+k];
	      
	      for(int k=0; k < NFields; k++)
		TecFile>>Dofs[NFields*a+k];
	    }
	  
	  for(int e=0; e<MD.elements*MD.nodes_element; e++)
	    TecFile>>MD.connectivity[e];
	}
      else
	{
	  std::cerr<<"\nMeshUtils.cpp-ReadTecplotFile() - Could not read mesh.";
	  exit(1);
	}
    }

  // Reading only coordinates and connectivity
  else
    {
      if(PositionCursor(TecFile, ET))
	{
	  for(int a=0; a<MD.nodes; a++)
	    for(int k=0; k < MD.spatial_dimension; k++)
	      TecFile>>MD.coordinates[MD.spatial_dimension*a+k];
	  
	  for(int e=0; e<MD.elements*MD.nodes_element; e++)
	    TecFile>>MD.connectivity[e];
	}
      else
	{
	  std::cerr<<"\nMeshUtils.cpp-ReadTecplotFile() - Could not read mesh.";
	  exit(1);
	}
    }
  
  TecFile.close();
  fflush( stdout );
  
  return Dofs;  // If NFields = 0, Dofs is an empty vector.
}





namespace{

struct facestructure
{
  int element;
  int face;
  std::vector<int> conn;

  facestructure(int e, int f, const std::vector<int> &co):
    element(e), face(f), conn(co) { sort(conn.begin(), conn.end()); }
  
  facestructure(const facestructure & newfacest): element(newfacest.element),
					    face(newfacest.face),
					    conn(newfacest.conn) {}

  bool operator<(const facestructure &other) const
  { return conn<other.conn; }
};
}

void GetCoordConnFaceNeighborList(const CoordConn & MD,
				  std::vector<std::vector<int>> &ElementNeighbors)
{
  ElementNeighbors.clear();
  ElementNeighbors.resize(MD.elements);
  
  int nFaces;
  std::vector<int> faceNum;
  
  if(MD.nodes_element == 4) // tetrahedral mesh
    {
      nFaces = 4;
      int fn[] = {2,1,0,
		  2,0,3,
		  2,3,1,
		  0,1,3};
      faceNum.assign(fn, fn+12);
    }
  
  else if(MD.nodes_element == 3) // triangular mesh
    {
      nFaces = 3;
      int fn[] = {0,1,
		  1,2,
		  2,0};
      faceNum.assign(fn, fn+6);
    }
  else
    {
      std::cout<<"\nMeshUtils.cpp-GetCoordConnFaceNeighborList():  "
	       <<"Unknown element.\n";
      exit(1);
    }
      
  const int neNodes = MD.nodes_element;     // nodes per element.
  const int nfNodes = MD.nodes_element-1;   // number of nodes per face.
  std::vector<facestructure> FacesStruct;
  
  for(int e=0; e<MD.elements; e++)
    {
      ElementNeighbors[e].resize(2*nFaces);
      
      for(int f=0; f<nFaces; f++)
	{
	  std::vector<int> conn(nfNodes);
	  for(int k=0; k<nfNodes; k++)
	    conn[k] = MD.connectivity[neNodes*e+faceNum[nfNodes*f+k]];
	  
	  facestructure *myfacestruct = new facestructure(e,f,conn);
	  FacesStruct.push_back(*myfacestruct);
	  
	  delete myfacestruct;
	}
    }

  sort(FacesStruct.begin(), FacesStruct.end());
  
  // Faces that have exactly the same connectivity should appear consecutively
  // If cannot find a pair, the face has no neighbor.
  
  for(std::vector<facestructure>::iterator it=FacesStruct.begin();
      it != FacesStruct.end(); it++)
    {
      if((it+1)!=FacesStruct.end())
	{
	  if(it->conn==(it+1)->conn)
	    {
	      ElementNeighbors[it->element][2*(it->face)] = (it+1)->element;
	      ElementNeighbors[it->element][2*(it->face)+1] = (it+1)->face;
	      ElementNeighbors[(it+1)->element][2*((it+1)->face)] = it->element;
	      ElementNeighbors[(it+1)->element][2*((it+1)->face)+1] = it->face;
	      
	      // Skip next element
	      it++;
	    }
	  else
	    ElementNeighbors[it->element][2*(it->face)] = -1;
	}
      else
	ElementNeighbors[it->element][2*(it->face)] = -1; 
    }
}





void GetCoordConnFaceNeighborList(const std::vector<int> &connectivity, 
				  const int nodes_element, 
				  const int spatial_dimension, 
				  std::vector<std::vector<int>> &ElementNeighbors)
{
  const int elements = int(connectivity.size())/nodes_element;
  ElementNeighbors.clear();
  ElementNeighbors.resize(elements);
  
  int nFaces;
  std::vector<int> faceNum;
  
  if(nodes_element == 4) // tetrahedral mesh
    {
      nFaces = 4;
      int fn[] = {2,1,0,
		  2,0,3,
		  2,3,1,
		  0,1,3};
      faceNum.assign(fn, fn+12);
    }
  
  else if(nodes_element == 3) // triangular mesh
    {
      nFaces = 3;
      int fn[] = {0,1,
		  1,2,
		  2,0};
      faceNum.assign(fn, fn+6);
    }
  else
    {
      std::cout<<"\nMeshUtils.cpp-GetCoordConnFaceNeighborList():  "
	       <<"Unknown element.\n";
      exit(1);
    }
      
  const int neNodes = nodes_element;     // nodes per element.
  const int nfNodes = nodes_element-1;   // number of nodes per face.
  std::vector<facestructure> FacesStruct;

  for(int e=0; e<elements; e++)
    {
      ElementNeighbors[e].resize(2*nFaces);
      
      for(int f=0; f<nFaces; f++)
	{
	  std::vector<int> conn(nfNodes);
	  for(int k=0; k<nfNodes; k++)
	    conn[k] = connectivity[neNodes*e+faceNum[nfNodes*f+k]];
	  
	  facestructure *myfacestruct = new facestructure(e,f,conn);
	  FacesStruct.push_back(*myfacestruct);
	  
	  delete myfacestruct;
	}
    }

  sort(FacesStruct.begin(), FacesStruct.end());
  
  // Faces that have exactly the same connectivity should appear consecutively
  // If cannot find a pair, the face has no neighbor.
  
  for(std::vector<facestructure>::iterator it=FacesStruct.begin();
      it != FacesStruct.end(); it++)
    {
      if((it+1)!=FacesStruct.end())
	{
	  if(it->conn==(it+1)->conn)
	    {
	      ElementNeighbors[it->element][2*(it->face)] = (it+1)->element;
	      ElementNeighbors[it->element][2*(it->face)+1] = (it+1)->face;
	      ElementNeighbors[(it+1)->element][2*((it+1)->face)] = it->element;
	      ElementNeighbors[(it+1)->element][2*((it+1)->face)+1] = it->face;
	      
	      // Skip next element
	      it++;
	    }
	  else
	    ElementNeighbors[it->element][2*(it->face)] = -1;
	}
      else
	ElementNeighbors[it->element][2*(it->face)] = -1; 
    }
}

