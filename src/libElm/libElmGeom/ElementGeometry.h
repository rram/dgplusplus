/*
 * ElementGeometry.h
 * DG++
 *
 * Created by Adrian Lew on 9/4/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef ELEMENTGEOMETRY
#define ELEMENTGEOMETRY

#include <string>
#include <vector>


/**
   \brief ElementGeometry: Defines the geometry of the polytope over which the
   interpolation takes place
   
   ElementGeometry consists of:\n
   1) A set of vertices that define a convex hull, the domain of the polytope\n
   2) A map from a parametric, reference polytope to the real
   polytope. This map is one-to-one, and may have domain and range
   in Euclidean spaces of different dimensions. In this way it is
   possible to map the parametric configuration of a planar
   triangle into three-dimensional real space as needed for plates
   and shells.\n
   3) A name for the polytope, for identification purposes whenever needed.\n
   
   The idea of the class is to avoid having copies of the vertices coordinates in the object,
   only the connectivity.
*/


class ElementGeometry
{
 public:
  inline ElementGeometry(){}
  inline virtual ~ElementGeometry(){}
  inline ElementGeometry(const ElementGeometry &){}
  virtual ElementGeometry* Clone() const = 0;
  virtual int GetNumberOfVertices() const = 0; 

  //!< Vertices of the polytope. 
  virtual const std::vector<int>& GetConnectivity() const = 0; 

  //! Name of type of polytope. 
  //! It clarifies the meaning of the connectivity array.
  virtual std::string GetPolytopeName() const = 0; 
  
  //! Number of dimensions in parametric configuration
  virtual int GetParametricDimension() const = 0; 

  //! Number of dimensions in the real configuration
  virtual int GetEmbeddingDimension() const = 0;

  //! Map from parametric to real configuration
  //! @param X parametric coordinates
  //! @param Y returned real coordinates
  virtual void Map(const double* X, double* Y) const = 0; 
  
  //! Derivative of map from parametric to real configuration
  //! @param X parametric coordinates.
  //! @param Jac returns absolute value of the Jacobian of the map.
  //! @param DY  returns derivative of the map. 
  //! Here DY[a*GetEmbeddingDimension()+i] 
  //! contains the derivative in the a-th direction
  //! of the i-th coordinate.
  virtual void DMap(const double* X, double* DY, double& Jac) const = 0; 

  //! Consistency test for Map and its derivative
  //! @param X parametric coordinates at which to test
  //! @param Pert size of the perturbation with which to compute numerical 
  //! derivatives (X->X+Pert)
  bool ConsistencyTest(const double* X, const double Pert) const;

  //! Number of faces the polytope has
  virtual int GetNumberOfFaces() const = 0;

  //! Creates and returns a new ElementGeometry object corresponding
  //! to face "e" in the polytope. The object has to be destroyed
  //! with delete by the recipient.
  //! 
  //! @param e face number, starting from 0
  //!
  //! Returns a null pointer if "e" is out of range
  virtual ElementGeometry* GetFaceGeometry(const int e) const = 0;
};

#endif
