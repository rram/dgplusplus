// Sriramajayam

// Purpose: To check P13DElement.


#include "P13DElement.h"
#include <iostream>
#include <vector> 

int main()
{ 
  std::vector<double> Vertices0({1,0,0, 0,1,0,0,0,0, 0,0,1});
  
  Tetrahedron::SetGlobalCoordinatesArray(Vertices0);

  P13DElement<2> TestElement(1,2,3,4);

  Element * VirtualElement;
  
  std::cout << "Number of fields: " << TestElement.GetNumFields() << " should be 2\n";
  std::cout << "Number of dof field(0): " << TestElement.GetDof(0) << " should be 4\n";
  std::cout << "Number of dof field(1): " << TestElement.GetDof(1) << " should be 4\n";
  for(int a=0; a<2; a++)
    {
      std::cout << "Shape function values at quad points field("<< a<< "):\n";
      for(unsigned int q=0;q<TestElement.GetShape(a).size();q++)
	std::cout << TestElement.GetShape(a)[q] << " ";
      std::cout << "\n";
    }
  
  for(int a=0; a<2; a++)
    {
      std::cout << "Shape function derivatives values at quad points field("<< a<< "):\n";
      for(unsigned int q=0;q<TestElement.GetDShape(a).size();q++)
        std::cout << TestElement.GetDShape(a)[q] << " ";
      std::cout << "\n";
    }
  
  for(int a=0; a<2; a++)
    {
      std::cout << "Integration weight values at quad points field("<< a<< "):\n";
      for(unsigned int q=0;q<TestElement.GetIntegrationWeights(a).size();q++)
	std::cout << TestElement.GetIntegrationWeights(a)[q] << " ";
      std::cout << "\n";
    }
  
  for(int a=0; a<2; a++)
    {
      std::cout << "Quad points coordinates for field("<< a<< "):\n";
      for(unsigned int q=0;q<TestElement.GetIntegrationPointCoordinates(a).size();q++)
	std::cout << TestElement.GetIntegrationPointCoordinates(a)[q] << " ";
      std::cout << "\n";
    }


  // testing CopyConstructor : //
  {
    P13DElement<2> CopyElement(TestElement);
    std::cout <<  "Test Copy Constructor\n";
    
    std::cout<<"Number of fields: " << CopyElement.GetNumFields() << " should be 2\n";
    std::cout<<"Number of dof field(0): " <<CopyElement.GetDof(0)<< " should be 4\n";
    std::cout<<"Number of dof field(1): " <<CopyElement.GetDof(1)<< " should be 4\n";

    for(int a=0; a<2; a++)
      {
	std::cout << "Shape function values at quad points field("<< a<< "):\n";
	for(unsigned int q=0;q<CopyElement.GetShape(a).size();q++)
	  std::cout << CopyElement.GetShape(a)[q] << " ";
	std::cout << "\n";
      }
    
    for(int a=0; a<2; a++)
      {
	std::cout << "Shape function derivatives values at quad points field("<< a<< "):\n";
	for(unsigned int q=0;q<CopyElement.GetDShape(a).size();q++)
	  std::cout << CopyElement.GetDShape(a)[q] << " ";
	std::cout << "\n";
      }
    
    for(int a=0; a<2; a++)
      {
	std::cout << "Integration weight values at quad points field("<< a<< "):\n";
        for(unsigned int q=0;q<CopyElement.GetIntegrationWeights(a).size();q++)
	  std::cout << CopyElement.GetIntegrationWeights(a)[q] << " ";
	std::cout << "\n";
      }
    
    for(int a=0; a<2; a++)
      {
	std::cout << "Quad points cooridnates for field("<< a<< "):\n";
	for(unsigned int q=0;q<CopyElement.GetIntegrationPointCoordinates(a).size();q++)
          std::cout << CopyElement.GetIntegrationPointCoordinates(a)[q] << " ";
	std::cout << "\n";
      }
    
    
    // Cloning mechanism : //
    VirtualElement = CopyElement.Clone();
    std::cout << "Cloned element before destruction. Test cloning mechanism\n";
  }
  
  std::cout <<"Number of fields: " << VirtualElement->GetNumFields() << " should be 2\n";
  std::cout <<"Number of dof field(0): "<<VirtualElement->GetDof(0)<<" should be 4\n";
  std::cout <<"Number of dof field(1): "<<VirtualElement->GetDof(1)<<" should be 4\n";
  
  for(int a=0; a<2; a++)
    {
      std::cout << "Shape function values at quad points field("<< a<< "):\n";
      for(unsigned int q=0;q<VirtualElement->GetShape(a).size();q++)
        std::cout << VirtualElement->GetShape(a)[q] << " ";
      std::cout << "\n";
    }
  
  for(int a=0; a<2; a++)
    {
      std::cout << "Shape function derivatives values at quad points field("<< a<< "):\n";
      for(unsigned int q=0;q<VirtualElement->GetDShape(a).size();q++)
	std::cout << VirtualElement->GetDShape(a)[q] << " ";
      std::cout << "\n";
    }
  
  for(int a=0; a<2; a++)
    {
      std::cout << "Integration weight values at quad points field("<< a<< "):\n";
      for(unsigned int q=0;q<VirtualElement->GetIntegrationWeights(a).size();q++)
	std::cout << VirtualElement->GetIntegrationWeights(a)[q] << " ";
      std::cout << "\n";
    }
  
  for(int a=0; a<2; a++)
    {
      std::cout << "Quad points cooridnates for field("<< a<< "):\n";
      for(unsigned int q=0;q<VirtualElement->GetIntegrationPointCoordinates(a).size();q++)
	std::cout << VirtualElement->GetIntegrationPointCoordinates(a)[q] << " ";
      std::cout << "\n";
    }
  
  delete VirtualElement;


}

