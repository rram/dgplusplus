/*
 * ForceField.h
 * DG++
 *
 * Created by Adrian Lew on 09/12/07.
 *  
 * Copyright (c) 2007 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef FORCEFIELD
#define FORCEFIELD

#include <vector>
#include <string>

/** 
    \brief Base class for force fields
    
    A force field is a function of the form
    \f[
    F({\bf u},{\bf X}),
    \f]
    where \f${\bf u}\f$ and \f${\bf X}\f$ are two sets of
    parameters. In principle there is no reason not to have just a
    single set of parameters. However, we shall understand \f${\bf
    u}\f$ as the set of unknown fields in the problem, and \f${\bf
    X}\f$ as, for example, spatial positions. It is useful to keep
    them as separate concepts, since it also provides a way to
    distiguish between variables for which derivatives are provided
    (\f$u\f$) and those for which they are not (\f$X\f$).  The number
    of components of \f$F\f$ is independent of those of \f$u\f$ and
    \f$X\f$.
*/
class ForceField
{
 public:
  inline virtual ~ForceField() {}
  virtual ForceField * Clone() const = 0;  
  
  //! Computes the value of the force field.
  //! Returns true if successful.
  //! @param u  vector of input values
  //! @param X  vector of input position values
  //! @param ForceFieldVal pointer to the vector with output values for forces.
  //! ForceFieldVal[i] contains the i-th component of the force. If the array does not 
  //! have enough space for all the components, it is resized.
  virtual bool GetForceField(const std::vector<double>  u, 
			     const std::vector<double>  X,
			     std::vector<double> *ForceFieldVal) const = 0;

  //! Returns the expected length of the vector u
  virtual unsigned int GetULength() const = 0;

  //! Returns the expected length of the vector X
  virtual unsigned int GetXLength() const = 0;

  //! Returns the expected length of the Force vector (output)
  virtual unsigned int GetForceLength() const = 0;
  
  //! Returns the name of the force field
  virtual const std::string GetForceFieldName() const = 0;
};


/** 
    \brief Base class for force fields with derivatives
    
    It consists of a force field for which derivatives are provided.

    A force field is a function of the form
    \f[
    F({\bf u},{\bf X}),
    \f]

    Only the derivatives with respect to \f${\bf u}\f$ are given.
    This is useful since in the traditional case the derivatives with
    respect to position \f${\bf X}\f$ are not needed. 
*/
class DForceField: public ForceField
{
 public:
  inline virtual ~DForceField() {}
  virtual DForceField * Clone() const = 0;  
  
  //! Computes the value of the force field
  //! Returns true if successful.
  //! @param u  vector of input values
  //! @param X  vector of input position values
  //! @param ForceFieldVal pointer to the vector with output values for forces.
  //! ForceFieldVal[i] contains the i-th component of the force. If the array does not 
  //! have enough space for all the components, it is resized.
  //! @param DForceFieldVal pointer to the vector with output values for force derivatives.
  //! DForceFieldVal[i*GetULength()+a] contains the derivative of ForceFieldVal[i]
  //!  with respect to u[a]. 
  //! If the vector does not have the appropriate size, it is resized.
  //! If argument not provided (=0), the derivative is not computed.

  virtual bool GetDForceField(const std::vector<double> u, 
			      const std::vector<double> X,
			      std::vector<double> *ForceFieldVal,
			      std::vector<double> *DForceFieldVal) const = 0;

  //! Consistency test\n
  //! Checks that the derivatives are right.
  //! @param FF Force field to test
  //! @param u values where to perform the test
  //! @param X values where to perform the test
  static bool ConsistencyTest(const DForceField & FF,
			      const std::vector<double> &u,
			      const std::vector<double> &X);
};


#endif
