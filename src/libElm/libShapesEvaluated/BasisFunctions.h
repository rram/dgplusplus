/*
 * BasisFunctions.h
 * DG++
 *
 * Created by Adrian Lew on 10/21/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef BASISFUNCTIONS
#define BASISFUNCTIONS

#include "Shape.h"
#include "Quadrature.h"
#include "ElementGeometry.h"

/**
   \brief 
   BasisFunctions: Evaluation of basis functions and derivatives at 
   the quadrature points. Abstract class.

   A BasisFunctions object consists of:\n
   1) A set of quadrature points with quadrature weights\n
   2) A set of basis functions and their derivatives evaluated at these points\n

   
   \todo So far the number of spatial dimensions, needed to transverse 
   the GetDShapes and GetQuadraturePointArrays is not provided, but should be 
   obtained from the ElementGeometry where these basis functions are in. One 
   way in which this may be computed is using the fact that 
   GetQuadraturePointCoordinates().size()/GetIntegrationWeights().size() = 
   GetDShapes().size()/GetShapes().size() = spatial dimensions

*/

class BasisFunctions
{
 public:
  inline BasisFunctions() {}
  inline virtual ~BasisFunctions(){}
  inline BasisFunctions(const BasisFunctions &) {}
  virtual BasisFunctions* Clone() const = 0;
  //!  Shape functions at quadrature points
  //!  GetShapes()[q*GetBasisDimension()+a]
  //!  gives the value of shape function a at quadrature point q
  //!  
  //!  GetShapes returns an empty vector if no shape functions are available
  virtual const std::vector<double>& GetShapes() const = 0; 
  
  //! Derivatives of shape functions at quadrature points 
  //! GetDShapes()[q*GetBasisDimension()*GetNumberOfDerivativesPerFunction()+
  //! +a*GetNumberOfDerivativesPerFunction()+i] gives the
  //! derivative in the i-th direction of degree of freedom a at quadrature point q
  //!
  //! GetDShapes returns an empty vector if no derivatives are
  //! available
  virtual const std::vector<double>& GetDShapes() const = 0; 
  
  virtual const std::vector<double>& GetIntegrationWeights() const = 0;  //!< Integration weights 

  //! Coordinates of quadrature points in the real configuration
  //! GetQuadraturePointCoordinates()
  //! [q*ElementGeometry::GetEmbeddingDimension()+i]
  //! returns the i-th coordinate in real space of quadrature point q 
  virtual const std::vector<double>& GetQuadraturePointCoordinates() const = 0; 

  //! returns the number of shape functions provided
  virtual int GetBasisDimension() const = 0;

  //! returns the number of directional derivative for each shape function
  virtual int GetNumberOfDerivativesPerFunction() const = 0;

  //! returns the number of  number of coordinates for each Gauss point
  virtual int GetSpatialDimensions() const = 0;
};




/**
   \brief dummy set with no basis functions.

   This class contains only static data and has the mission of providing 
   a BasisFunctions object that has no basis functions in it.

   This becomes useful, for example, as a cheap way of providing Element 
   with a BasisFunction object that can be returned but that occupies no memory.
   Since Element has to be able to have a BasisFunction object per field, by utilizing
   this object there is no need to construct an odd order for the fields in order to 
   save memory.
   
 */
class EmptyBasisFunctions: public BasisFunctions
{
 public:
  inline EmptyBasisFunctions() {}
  inline virtual ~EmptyBasisFunctions(){}
  inline EmptyBasisFunctions(const EmptyBasisFunctions &) {}
  inline EmptyBasisFunctions* Clone() const override
  { return new EmptyBasisFunctions(*this); }
  inline const std::vector<double>& GetShapes() const override
  { return ZeroSizeVector; }

  inline const std::vector<double>& GetDShapes() const override
  { return ZeroSizeVector; }

  inline const std::vector<double>& GetIntegrationWeights() const override
  { return ZeroSizeVector; }
  
  inline const std::vector<double>& GetQuadraturePointCoordinates() const override
  { return ZeroSizeVector; }

  inline int GetBasisDimension() const override
  { return 0; }

  inline int GetNumberOfDerivativesPerFunction() const override
  { return 0; } 

  inline int GetSpatialDimensions() const override
  { return 0; }
   
 private:
  static std::vector<double> ZeroSizeVector;
};

#endif
