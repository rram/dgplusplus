/*
 * LinearForceField.h
 * DG++
 *
 * Created by Adrian Lew on 09/12/07.
 *  
 * Copyright (c) 2007 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef LINEARFORCEFIELD
#define LINEARFORCEFIELD

#include "ForceField.h"
#include <string>
#include <cstdlib>
#include <cassert>

#define GREATER(X,Y)   ((X)>(Y)? (X): (Y))


/** 
    \brief Linear force field per unit volume.

    General expression for the force per unit volume
    \f[
    F({\bf X}) = {\bf g}_u \cdot {\bf u} + {\bf g}_X \cdot {\bf X}
    \f]
    or
    \f[
    F({\bf X}) = (g_u)_{ia} \cdot u_a + ({g}_X)_{iK} X_K
    \f]

    where \f${\bf g}_u\f$ and \f${\bf g}_X\f$ are matrices independent
    of position or field values. 
    
    Let uLen be the number of components of \f${\bf u}\f$.\n
    Let XLen be the number of components of \f${\bf X}\f$.\n
    Let FLen be the number of components of \f${\bf F}\f$.\n
    Then\n
    \f${\bf g}_u\f$ is a matrix of dimensions Flen*ulen, and \n
    \f${\bf g}_X\f$ is a matrix of dimensions Flen*Xlen, and \n
*/
class LinearForceField: public DForceField
{
 public:
  //! @param uGrad: vector with the components of \f${\bf g}_u\f$.
  //! uGrad[i*ulen+a] gives \f$({g}_u)_{ia}\f$.  
  //! Copied, not referenced
  //! @param XGrad: vector with the components of \f${\bf g}_X\f$. 
  //! XGrad[i*xlen+K] gives \f$({g}_X)_{iK}\f$.
  //! Copied, not referenced
  //! @param ulen: number of components of \f${\bf u}\f$
  //! @param Xlen: number of components of \f${\bf X}\f$
  //! The following conditions need to be met:\n
  //! 1) If ulen!=0 then uGrad.size()/ulen needs be an integer
  //! 2) If Xlen!=0 then XGrad.size()/Xlen needs be an integer
  //! 3) If ulen!=0 and Xlen !=0 then uGrad.size()/ulen=XGrad.size()/Xlen 
  //! 
  //! The number of components of $F$ is then either uGrad.size()/ulen or XGrad.size()/Xlen 
  //! 
  //! If either ulen or Xlen are zero, then uGrad or XGrad can be a zero length vector, 
  //! respectively.

  inline LinearForceField(const std::vector<double>& uGrad,
			  const std::vector<double>& XGrad,
			  const int ulen,
			  const int Xlen): 
  LocalUGrad(uGrad), LocalXGrad(XGrad), LocalULen(ulen), LocalXLen(Xlen) 
  {
    assert((ulen!=0 || Xlen!=0)  && "Linear force field: Incompatible sizes");
    if(ulen!=0)
      assert(uGrad.size()%ulen==0 && "Linear force field: Incompatible sizes");
    if(Xlen!=0)
      assert(XGrad.size()%Xlen==0 && "Linear force field: Incompatible sizes");
    
    int LFu=0;
    int LFX=0;
    if(ulen!=0)
      LFu = uGrad.size()/ulen;
    if(Xlen!=0)
      LFX = XGrad.size()/Xlen;
    
    LocalFLen = GREATER(LFu,LFX);
  }
  
  inline virtual ~LinearForceField() {}
  
 LinearForceField(const LinearForceField & OldLFF):
  LocalUGrad(OldLFF.LocalUGrad), LocalXGrad(OldLFF.LocalXGrad), 
    LocalULen(OldLFF.LocalULen), LocalXLen(OldLFF.LocalXLen), LocalFLen(OldLFF.LocalFLen) {}
  
  virtual LinearForceField * Clone() const { return new LinearForceField(*this); }  

  inline bool GetForceField(const std::vector<double> uVal, 
			    const std::vector<double> XVal,
			    std::vector<double> *ForceFieldVal) const
  { return GetDForceField(uVal, XVal, ForceFieldVal, 0); }
  
  inline bool GetDForceField(const std::vector<double> u, 
			     const std::vector<double> X,
			     std::vector<double> *ForceFieldVal,
			     std::vector<double> *DForceFieldVal) const
  { 
    unsigned int ulen = GetULength();
    unsigned int Xlen = GetXLength();
    unsigned int Flen = GetForceLength();

    if(ForceFieldVal->size()<Flen) ForceFieldVal->resize(Flen);
    
    for(unsigned int i=0; i<Flen; i++) 
      {
	(*ForceFieldVal)[i] = 0;

	for(unsigned int a=0; a<ulen; a++) 
	  (*ForceFieldVal)[i] += LocalUGrad[i*ulen+a]*u[a];

	for(unsigned int K=0; K<Xlen; K++) 
	  (*ForceFieldVal)[i] += LocalXGrad[i*Xlen+K]*X[K];
      }
    
    if(DForceFieldVal!=0)
      {
	if(DForceFieldVal->size()<Flen*ulen) DForceFieldVal->resize(Flen*ulen);

	for(unsigned int i=0; i<Flen; i++) 
	  for(unsigned int a=0; a<ulen; a++) 
	    (*DForceFieldVal)[i*ulen+a] = LocalUGrad[i*ulen+a];
      }

    return true;
  }
  
  //! Returns the expected lengths of uVal
  inline unsigned int GetULength() const { return LocalULen; }

  //! Returns the expected lengths of XVal
  inline unsigned int GetXLength() const { return LocalXLen; }
  
  //! Returns the expected length of the Force vector (output)
  virtual unsigned int GetForceLength() const { return LocalFLen;}

  //! Returns the name of the force field
  const std::string GetForceFieldName() const { return "LinearForceField"; }

 private:
  std::vector<double> LocalUGrad;
  std::vector<double> LocalXGrad;
  unsigned int LocalULen;
  unsigned int LocalXLen;
  unsigned int LocalFLen;
};


#endif
