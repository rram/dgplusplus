// Sriramajayam

// Purpose :  To check ShapesEvaluatedP13D

#include "Tetrahedron.h"
#include "ShapesEvaluated.h"
#include "Linear.h"
#include "Quadrature.h"
#include <iostream>

static void PrintData(ShapesEvaluated& PShapes);

int main()
{
  const int bctMap[] = {0,1,3,2};
  Linear<3> Shp(bctMap);
  const auto* Quad = Tet_1::Bulk;
  
  std::vector<double> Vertices0({1,0,0, 
	0,1,0, 
	0,0,0, 
	0,0,1});
  Tetrahedron::SetGlobalCoordinatesArray(Vertices0);
  Tetrahedron P1(1,2,3,4);
  ShapesEvaluated P1Shapes(Quad, &Shp, &P1);

  std::cout << "Parametric tet\n";
  PrintData(P1Shapes);

  std::cout << "\nTwice Parametric tet\n";
  std::vector<double> Vertices1({2,0,0, 
			    0,2,0, 
			    0,0,0, 
	0,0,2});
  Tetrahedron::SetGlobalCoordinatesArray(Vertices1);
  Tetrahedron P2(1,2,3,4);
  ShapesEvaluated P2Shapes(Quad, &Shp, &P2);
  PrintData(P2Shapes);
}




static void PrintData(ShapesEvaluated& PShapes)
{
  std::cout << "Function values\n";
  for(unsigned int a=0; a<PShapes.GetShapes().size(); a++)
    std::cout <<  PShapes.GetShapes()[a] << " ";
  std::cout << "\n";
  
  std::cout << "Function derivative values\n";
  for(unsigned int a=0; a<PShapes.GetDShapes().size(); a++)
    std::cout <<  PShapes.GetDShapes()[a] << " ";
  std::cout << "\n";
  
  std::cout << "Integration weights\n";
  for(unsigned int a=0; a<PShapes.GetIntegrationWeights().size(); a++)
    std::cout <<  PShapes.GetIntegrationWeights()[a] << " ";
  std::cout << "\n";
  
  std::cout << "Quadrature point coordinates\n";
  for(unsigned int a=0; a<PShapes.GetQuadraturePointCoordinates().size(); a++)
    std::cout <<  PShapes.GetQuadraturePointCoordinates()[a] << " ";
  std::cout << "\n";
}

