// Sriramajayam

#include <StressWork.h>
#include <BodyForceWork.h>
#include <cmath>
#include <iostream>

// Anonymous namespace
namespace
{
  bool ConsistencyTest_(const DResidue* Op,
			const VectorMapAccess& VMAccess, 
			const void* arg, 
			const double pertEPS, const double tolEPS)
  {
    const VectorMap* ptr = static_cast<const VectorMap*>(arg);
    assert(ptr!=nullptr && "ConsistencyTest_: Invalid pointer to displacement map");
    const VectorMap& DispMap = *ptr;
    assert(DispMap.IsInitialized() && "ConsistencyTest_: Displacement not initialized");
    const int SPD = static_cast<int>(Op->GetField().size());

    // Local node numbers to access global displacement fields
    const auto& LocNodes = VMAccess.Get();
   
    // Fields and their dofs
    const auto& Fields = Op->GetField();
    const int NFields = static_cast<int>(Fields.size());
    std::vector<int> nDofs(NFields);
    for(int f=0; f<NFields; ++f)
      nDofs[f] = Op->GetFieldDof(f);
  
    // Size arrays
    std::vector<std::vector<double>>
      res(NFields), resnum(NFields), resplus(NFields), resminus(NFields);
    std::vector<std::vector<std::vector<std::vector<double>>>> dres(NFields), dresnum(NFields);
    for(int f=0; f<NFields; ++f)
      {
	res[f].resize(nDofs[f]);
	resnum[f].resize(nDofs[f]);
	resplus[f].resize(nDofs[f]);
	resminus[f].resize(nDofs[f]);
	dres[f].resize(nDofs[f]);
	dresnum[f].resize(nDofs[f]);

	for(int a=0; a<nDofs[f]; ++a)
	  {
	    dres[f][a].resize(NFields);
	    dresnum[f][a].resize(NFields);
	  for(int g=0; g<NFields; ++g)
	    {
	      dres[f][a][g].resize(nDofs[g]);
	      dresnum[f][a][g].resize(nDofs[g] );
	    }
	}
    }

  // Correct residual and stiffness
    Op->GetDVal(&DispMap, &res, &dres);

    // Compute numerical approx of stiffness
    for(int f=0; f<NFields; ++f)
      for(int a=0; a<nDofs[f]; ++a)
	{
	  // Positive perturbation 
	  VectorMap plusDispMap(DispMap);
	  std::vector<double> pert(SPD);
	  for(int k=0; k<SPD; ++k)
	    pert[k] = 0.;
	  pert[f] = pertEPS;
	  plusDispMap.Increment(LocNodes[a], &pert[0]);
	  plusDispMap.SetInitialized();
	  Op->GetVal(&plusDispMap, &resplus);
	  
	  // Negative perturbation
	  VectorMap minusDispMap(DispMap);
	  pert[f] *= -1.;
	  minusDispMap.Increment(LocNodes[a], &pert[0]);
	  minusDispMap.SetInitialized();
	  Op->GetVal(&minusDispMap, &resminus);
	  
	  // Numerical stiffness
	  for(int g=0; g<NFields; ++g)
	    for(int b=0; b<nDofs[g]; ++b)
	      dresnum[g][b][f][a] = (resplus[g][b]-resminus[g][b])/(2.*pertEPS);
	}

    // Consistency check
    for(int f=0; f<NFields; ++f)
      for(int a=0; a<nDofs[f]; ++a)
	for(int g=0; g<NFields; ++g)
	  for(int b=0; b<nDofs[g]; ++b)
	    if(std::abs(dres[f][a][g][b]-dresnum[f][a][g][b])>tolEPS)
	      return false;

    // For debugging purposes only
    if(0)
      for(int f=0; f<NFields; ++f)
	for(int a=0; a<nDofs[f]; ++a)
	  for(int g=0; g<NFields; ++g)
	    for(int b=0; b<nDofs[g]; ++b)
	      std::cout<<"\n"<<dres[f][a][g][b]<<" should be "<<dresnum[f][a][g][b];
  
  
  return true;
  }
}


// Consistency test for stress work
bool StressWork::ConsistencyTest(const void* arg,
				 const double pertEPS,
				 const double tolEPS) const
{ return ConsistencyTest_(this, this->VMAccess, arg, pertEPS, tolEPS); }

// Consistency test for body force work
bool BodyForceWork::ConsistencyTest(const void* arg,
				 const double pertEPS,
				    const double tolEPS) const
{ return ConsistencyTest_(this, this->VMAccess, arg, pertEPS, tolEPS); }
