/*
 * Shape.cpp
 * DG++
 *
 * Created by Adrian Lew on 9/7/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "Shape.h"
#include <cmath>
#include <cassert>

bool Shape::ConsistencyTest(const double * X, const double Pert) const
{
  assert(Pert>0. &&
	 "\nShape::ConsistencyTest - Pert cannot be less or equal than zero\n");
  
  const int nVars = GetNumberOfVariables();
  const int nFuncs = GetNumberOfFunctions();
  
  double *DValNum = new double[nFuncs*nVars];
  double *DValAnal = new double[nFuncs*nVars];
  double *Xpert = new double[nVars];
  double *Valplus = new double[nFuncs];
  double *Valminus = new double[nFuncs];

  
  for(int i=0; i<nVars; ++i) 
    {
      Xpert[i]=X[i];
      for(int a=0; a<nFuncs; a++) 
	DValAnal[a*nVars+i] = DVal(a, X, i);	
    }

  for(int i=0; i<nVars; ++i)
    {
      Xpert[i] = X[i] + Pert;
      for(int a=0; a<nFuncs; ++a) 
	Valplus[a] = Val(a,Xpert);

      Xpert[i] = X[i] - Pert;
      for(int a=0; a<nFuncs; ++a) 
	Valminus[a] = Val(a,Xpert);

      Xpert[i] = X[i];

      for(int a=0; a<nFuncs; ++a)
	DValNum[a*nVars+i] = (Valplus[a]-Valminus[a])/(2*Pert);
    }
  
  double error = 0;
  double normX = 0;
  double normDValNum = 0;
  double normDValAnal = 0;

  for(int i=0; i<nVars; ++i)
    {
      normX += X[i]*X[i];
      
      for(int a=0; a<nFuncs; ++a)
	{
	  error += pow(DValAnal[a*nVars+i]-DValNum[a*nVars+i],2.);
	  normDValAnal += pow(DValAnal[a*nVars+i],2.);
	  normDValNum += pow(DValNum[a*nVars+i],2.);
	}
    }
  error = sqrt(error);
  normX = sqrt(normX);
  normDValAnal = sqrt(normDValAnal);
  normDValNum = sqrt(normDValNum);

  delete[] Valplus;
  delete[] Valminus;
  delete[] Xpert;
  delete[] DValNum;
  delete[] DValAnal;

  if(error*(normX+Pert) < (normDValAnal<normDValNum? normDValNum: normDValAnal)*Pert*10)
    return true;
  else
    return false;
}


