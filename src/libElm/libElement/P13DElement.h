/*
 * P13DElement.h
 * DG++
 *
 * Created by Ramsharan Rangarajan
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef P13DELEMENT
#define P13DELEMENT

#include "P12DElement.h"
#include "Tetrahedron.h"
#include "Linear.h"

//! \brief three-dimensional linear tetrahedra with NField different fields.
template<int NFields> 
class P13DElement : public Element
{
 public:
  //! \param i1 vertex 0
  //! \param i2 vertex 1
  //! \param i3 vertex 2
  //! \param i4 vertex 3
  P13DElement(int i1, int i2, int i3, int i4)
    {
      TetGeom = new Tetrahedron(i1,i2,i3,i4);
      int perm[] = {0,1,3,2};
      Linear<3> Lin(perm);
      ShapesEvaluated ModelShape(Tet_1::Bulk, &Lin, TetGeom);
      AddBasisFunctions(ModelShape);
      for(int i=0; i<NFields; ++i)
	AppendField(i);
    }

  virtual ~P13DElement() { delete TetGeom; }

  //! Copy constructor
  P13DElement(const P13DElement<NFields> &Elm)
    :Element(Elm)
  { TetGeom = new Tetrahedron(*Elm.TetGeom); }

    //! Cloning
  inline virtual P13DElement<NFields>* Clone() const override
  { return new P13DElement<NFields>(*this); }
  

  //! Access to ElementGeometry
  virtual const Tetrahedron & GetElementGeometry() const override
  { return *TetGeom; }


 protected:
  //! Mapping from field to its index in this element
  //! Multiple fields can use the same basis functions.
  //! Therefore, the index need not be consistent with the member Fields_
  //! \param[in] field Global field number
  //! \return Index of given field
  virtual int GetFieldIndex(int field) const override
  { return 0; }
  
 private:
  Tetrahedron *TetGeom;
};




/**
   \brief Standard local to global map when only P13D type elements are used.
   
   (almost verbatim from StandardP12DMap)
   StandardP13DMap assumes that
   1) The int of a node is an int
   2) All dofs are associated with nodes, and their values for each node ordered consecutively according to
   the field number.
   
   Consequently, the GlobalDofIndex of the dofs of a node N with NField fields are given by
   (N-1)*NFields + field where 0<= fields < NFields.
   
   \todo This map is identical to StandardP12DMap. Maybe StandardP12DMap should be renamed to be used here too.
*/
typedef StandardP12DMap StandardP13DMap;

/*
class StandardP13DMap: public StandardP12DMap
{
 public:
  inline StandardP13DMap(const std::vector<Element*> &EA)
    :StandardP12DMap(EA) {}
    
  inline virtual ~StandardP13DMap() {}
  
    inline StandardP13DMap(const StandardP13DMap &NewMap)
      :StandardP12DMap(NewMap) {}
      };*/

#endif



