/*
 * P12DElement.h
 * DG++
 *
 * Created by Adrian Lew on 9/22/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef P12DELEMENT
#define P12DELEMENT

#include "Element.h"
#include "Triangle.h"
#include "ShapesEvaluated.h"
#include "LocalToGlobalMap.h"
#include "Linear.h"

/**
   \brief Two-dimensional linear triangles with NFields different fields
*/

template<int NFields> class P12DElement: public Element
{
 public:
 P12DElement(int i1, int i2, int i3)
   :Element()
    {
      TriGeom = new Triangle<2>(i1,i2,i3);
      Linear<2> Shp;
      ShapesEvaluated ModelShape(Triangle_1::Bulk, &Shp, TriGeom);
      AddBasisFunctions(ModelShape);
      for(int i=0; i<NFields; ++i)
	AppendField(i);
    }
  
  virtual ~P12DElement() { delete TriGeom; }
  
 P12DElement(const P12DElement<NFields> &Elm)
   :Element(Elm)
  { TriGeom = new Triangle<2>(*Elm.TriGeom); }

  //! Cloning
  inline virtual P12DElement<NFields>* Clone() const override
  { return new P12DElement<NFields>(*this); }
  
  virtual const Triangle<2>& GetElementGeometry() const override
  { return *TriGeom; }
  
 protected:
  //! Mapping from field to its index in this element
  //! Multiple fields can use the same basis functions.
  //! Therefore, the index need not be consistent with the member Fields_
  //! \param[in] field Global field number
  //! \return Index of given field
  virtual int GetFieldIndex(int field) const override
  { return 0; }
  
 private:
  Triangle<2> *TriGeom;
};


/**
   \brief StandardP12DMap class: standard local to global map when only P12D type of 
   Elements are used. 
   
   StandardP12DMap assumes that\n
   1) The int of a node is an int\n
   2) All degrees of freedom are associated with nodes, and their values for each 
   node ordered consecutively according to the field number.
   
   Consequently, the GlobalDofIndex of the degrees of freedom of node N with NField 
   fields are given by
   
   (N-1)*NFields + field-1, where \f$ 1 \le \f$ fields \f$ \le \f$ NFields
*/
class StandardP12DMap: public LocalToGlobalMap
{
 public:
  //! @param EA array where the Element objects are. All GlobalElementIndex
  //! indices in this class refer to positions in EA. \n
  //! EA is not copied, only linked to. Of course, it is not destroyed upon
  //! destruction of StandardP12DMap objects.
  inline StandardP12DMap(const std::vector<Element*>& EA) 
  { ElementArray = &EA; }
  
  inline virtual ~StandardP12DMap() {}

  inline StandardP12DMap(const StandardP12DMap &NewMap) 
  { ElementArray = NewMap.ElementArray; }

  inline virtual StandardP12DMap* Clone() const override
  { return new StandardP12DMap(*this); }
  
  inline int Map(const int field, const int dof, const int elm) const override
    { 
      return 
	(*ElementArray)[elm]->GetNumFields()*
	((*ElementArray)[elm]->
	 GetElementGeometry().GetConnectivity()[dof]-1)
	+ field; 
    }
  
  
  inline int GetNumElements() const override
  { return (*ElementArray).size(); }

  inline int GetNumFields(const int elm) const override
    { return (*ElementArray)[elm]->GetNumFields(); }
  
  inline int GetNumDofs(const int elm, const int field) const override
    { return (*ElementArray)[elm]->GetDof(field); }
  
  inline int GetTotalNumDof() const override
  {
    int MaxNodeNumber = 0;
    int nNodes;
    const int nElements = static_cast<int>(ElementArray->size());
    for(int e=0; e<nElements; e++)
      {
	const auto& conn = (*ElementArray)[e]->GetElementGeometry().GetConnectivity();
	nNodes = static_cast<int>(conn.size());
	for(int a=0, nn=0; a<nNodes; a++)
	  MaxNodeNumber = (nn=conn[a])>MaxNodeNumber? nn:MaxNodeNumber;
      }
    return MaxNodeNumber*(*ElementArray)[0]->GetNumFields();
  }
  

 protected:
  //! Access to ElementArray  for derived classes
  const std::vector<Element*>& GetElementArray() const
  { return *ElementArray; }

 private:
  const std::vector<Element*>  *ElementArray;
};


#endif

