// Sriramajayam

#include "PlottingUtils.h"
#include <cstring>

void PlotTecCoordConn(const char * filename, const CoordConn &MD, const char* ET)
{ PlotTecCoordConnWithNodalFields(filename, MD, nullptr, 0, ET); }
  
void PlotTecCoordConnWithNodalFields(const char * filename, 
				     const CoordConn &MD, 
				     const double *nodalfieldtoplot,
				     const int NFields, 
				     const char* ET)
{
  // Open file to plot
  std::fstream outfile;
  outfile.open(filename, std::ios::out);
  
  // Line 1:
  if( MD.spatial_dimension==2 )
    outfile<<"VARIABLES = \"X\", \"Y\" ";
  else if( MD.spatial_dimension==3)
    outfile<<"VARIABLES = \"X\", \"Y\", \"Z\" ";
  
  for(int i=1; i<=NFields; i++)
    {
      char var1[100] = "F";
      char var2[100];
      sprintf(var2, "%d", i);
      strcat(var1, var2);
      outfile<<", \""<<var1<<"\" ";
    }
  outfile<<"\n";
  
  // Line 2:
  outfile<<"ZONE t=\"t:0\", N="<<MD.nodes<<", E="<<MD.elements<<", F=FEPOINT, ET=";
  if( ET==NULL)
    {
      if(MD.nodes_element==3)
	outfile<<"TRIANGLE";
      else if(MD.nodes_element==4 && MD.spatial_dimension==2)
	outfile<<"QUADRILATERAL";
      else if(MD.nodes_element==4)
	outfile<<"TETRAHEDRON";
      else if(MD.spatial_dimension==3 && MD.nodes_element==8)
	outfile<<"BRICK";
      else
	{
	  std::cerr<<"\nPlottingUtils.cpp- PlotTecCoordConnWithNodalFields()- "
		   <<"Unknown element type provided. \n";
	  exit(1);
	}
    }
  else
    outfile<<ET;
  outfile<<"\n";
  
  std::cout.precision(16);
  
  // Nodal coordinates and field values
  for(int i=0; i<MD.nodes; i++)
    {
      for(int k=0; k<MD.spatial_dimension; k++)
	outfile<<std::scientific<<MD.coordinates[i*MD.spatial_dimension+k]<<" ";
      
      if(nodalfieldtoplot!=0)
	for(int f=0; f<NFields; f++)
	  outfile<<std::scientific<<nodalfieldtoplot[i*NFields+f]<<" ";
      outfile<<"\n";
    }
  
  // Connectivity
  for(int e=0; e<MD.elements; e++)
    {
      for(int k=0; k<MD.nodes_element; k++)
	outfile<<MD.connectivity[e*MD.nodes_element+k]<<" ";
      outfile<<"\n";
    }
  
  outfile.close();
}


void PlotTecCoordConnElmList(const char * filename, 
			     const CoordConn &MD, 
			     const std::vector<int> &ElmList)
{
  CoordConn NewMD = MD;
  NewMD.connectivity.clear();
  NewMD.elements = int(ElmList.size());
  for(int e=0; e<NewMD.elements; e++)
    {
      int elm = ElmList[e];
      for(int a=0; a<MD.nodes_element; a++)
	NewMD.connectivity.push_back( MD.connectivity[MD.nodes_element*elm+a] );
    }
  PlotTecCoordConnWithNodalFields(filename, NewMD);
}
