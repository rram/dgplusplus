/*
 * ForceField.cpp
 * DG++
 *
 * Created by Adrian Lew on 09/14/07.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "ForceField.h"
#include <vector>
#include <cmath>
#include <iostream>

bool  DForceField::ConsistencyTest(const DForceField & FF,
				   const std::vector<double> &uVal ,
				   const std::vector<double> &XVal)
{
  int NFields = FF.GetForceLength();
  int ulen    = FF.GetULength();

  std::vector<double> luVal(uVal);
  std::vector<double> funcval(NFields);
  std::vector<double> funcvalplus(NFields);
  std::vector<double> funcvalminus(NFields);
  std::vector<double> dfuncval(NFields*ulen);
  std::vector<double> dfuncvalnum(NFields*ulen);

  double EPS = 1.e-6;  
  
  double maxval = 0;
  for(int u=0; u<ulen; u++)
    if(maxval>fabs(uVal[u]))
      maxval = fabs(uVal[u]);

  maxval += 1;

  for(int u=0; u<ulen; u++)
    {
      double ival = luVal[u];
      
      luVal[u] = ival+ EPS*maxval;
      FF.GetForceField(luVal, XVal, &funcvalplus);

      luVal[u] = ival - EPS*maxval;
      FF.GetForceField(luVal, XVal, &funcvalminus);

      luVal[u] = ival;
      
      for(int g=0; g<NFields; g++)
	dfuncvalnum[g*ulen+u] = 
	  (funcvalplus[g]-funcvalminus[g])/(2*EPS*maxval);
    }
  FF.GetDForceField(uVal, XVal, &funcval, &dfuncval);

      
  double error = 0;
  double norm = 0;
  for(unsigned int f=0; f<dfuncval.size(); f++)
    {
      error += pow(dfuncval[f]-dfuncvalnum[f],2);
      norm += pow(dfuncval[f],2);
    }
  
  error = sqrt(error);
  norm = sqrt(norm);
  
  if(error/norm>EPS*100)
    {
      std::cerr << "DResidue::ConsistencyTest. DResidue not consistent\n";
      std::cerr << "norm: " << norm << " error: " << error << "\n";
      return false;
    }
  return true;
}

