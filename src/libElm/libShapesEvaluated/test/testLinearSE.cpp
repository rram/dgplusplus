/*
 * testLinearSE.cpp
 * DG++
 *
 * Created by Adrian Lew on 9/9/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 


#include "Triangle.h"
#include "ShapesEvaluated.h"
#include "Quadrature.h"
#include "Linear.h"
#include <iostream>

static void PrintData(const ShapesEvaluated& P10Shapes);

int main()
{
  // Linear shape functions
  const Linear<2> Shp;

  // Quadrature
  const auto* Quad = Triangle_1::Bulk;
  
  std::vector<double> Vertices0({1,0,0,1,0,0});
  Triangle<2>::SetGlobalCoordinatesArray(Vertices0);
  Triangle<2> P10(1,2,3);
  ShapesEvaluated P10Shapes(Quad, &Shp, &P10);
  
  std::cout << "Parametric triangle\n";
  PrintData(P10Shapes);

  std::cout << "\nTwice Parametric triangle\n";

  std::vector<double> Vertices1({2,0,0,2,0,0});
  Triangle<2>::SetGlobalCoordinatesArray(Vertices1);
  Triangle<2> P11(1,2,3);
  ShapesEvaluated P11Shapes(Quad, &Shp, &P11);

  PrintData(P11Shapes);

  std::cout << "\nReordered nodes of  twice parametric triangle\n";

  std::vector<double> Vertices2({0,0,2,0,0,2});
  Triangle<2>::SetGlobalCoordinatesArray(Vertices2);
  Triangle<2> P12(1,2,3);
  ShapesEvaluated P12Shapes(Quad, &Shp, &P12);

  PrintData(P12Shapes);

  std::cout << "\n Equilateral triangle with area sqrt(3)\n";

  std::vector<double> Vertices3({0,0,2,0,1,sqrt(3)});
  Triangle<2>::SetGlobalCoordinatesArray(Vertices3);
  Triangle<2> P13(1,2,3);
  ShapesEvaluated P13Shapes(Quad, &Shp, &P13);

  PrintData(P13Shapes);

  std::cout << "\n Irregular triangle with area sqrt(3)\n";

  std::vector<double> Vertices4({0,0,2,0,2.5,sqrt(3)});
  Triangle<2>::SetGlobalCoordinatesArray(Vertices4);
  Triangle<2> P14(1,2,3);
  ShapesEvaluated P14Shapes(Quad, &Shp, &P14);

  PrintData(P14Shapes);

}




static void PrintData(const ShapesEvaluated& P10Shapes)
{
  std::cout << "Function values\n";
  for(unsigned int a=0; a<P10Shapes.GetShapes().size(); a++)
    std::cout <<  P10Shapes.GetShapes()[a] << " ";
  std::cout << "\n";
  
  std::cout << "Function derivative values\n";
  for(unsigned int a=0; a<P10Shapes.GetDShapes().size(); a++)
    std::cout <<  P10Shapes.GetDShapes()[a] << " ";
  std::cout << "\n";
  
  std::cout << "Integration weights\n";
  for(unsigned int a=0; a<P10Shapes.GetIntegrationWeights().size(); a++)
    std::cout <<  P10Shapes.GetIntegrationWeights()[a] << " ";
  std::cout << "\n";
  
  std::cout << "Quadrature point coordinates\n";
  for(unsigned int a=0; a<P10Shapes.GetQuadraturePointCoordinates().size(); a++)
    std::cout <<  P10Shapes.GetQuadraturePointCoordinates()[a] << " ";
  std::cout << "\n";
}
