/*
 * External.h
 * DG++
 *
 * Created by Adrian Lew on 10/30/06.
 *  
 * Copyright (c) 2006 Adrian Lew and Alex TenEyck
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef EXTERNAL
#define EXTERNAL

#include <iostream>
#include <fstream>
#include <iomanip>
#include "LocalToGlobalMap.h"
#include <vector>


struct CoordConn
{
  std::vector<double> coordinates;
  std::vector<int>    connectivity;
  int spatial_dimension;
  int nodes_element;
  int elements;
  int nodes;  
};


// Subdivision for Triangles:
void SubdivideTriangles(std::vector<int> &Conn, std::vector<double> &Coord, 
			int & nodes, int & elements);

// Subdivision for Tetrahedra:
void SubdivideTetrahedra(std::vector<int> &Conn, std::vector<double> &Coord, 
			 int & nodes, int & elements);     

#endif
