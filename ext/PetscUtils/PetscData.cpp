
/* Copyright (c) 2006 Stanford University and Alex TenEyck and Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "PetscData.h"
#include <cassert>

void PetscData::Initialize(const std::vector<int>& nz)
{
  const int ndof = static_cast<int>(nz.size());

  // Set the sizes, and options
  PetscErrorCode ierr;
  
  ierr = MatCreateSeqAIJ(PETSC_COMM_WORLD, ndof, ndof, PETSC_DEFAULT, &nz[0], &(stiffnessMAT)); CHKERRV(ierr);
  ierr = VecCreate(PETSC_COMM_WORLD,&(resVEC)); CHKERRV(ierr);
  ierr = VecSetSizes(resVEC,PETSC_DECIDE, ndof); CHKERRV(ierr);
  ierr = VecSetFromOptions(resVEC); CHKERRV(ierr);
  ierr = VecDuplicate(resVEC,&(solutionVEC)); CHKERRV(ierr);
  ierr = VecDuplicate(resVEC, &(DOFArray)); CHKERRV(ierr);

  // We can zero rows for dirichlet BCs without loosing the zero structure
  ierr = MatSetOption(stiffnessMAT,MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE); CHKERRV(ierr);
  
  // Solver
  ierr = KSPCreate(PETSC_COMM_WORLD,&(kspSOLVER)); CHKERRV(ierr);
  ierr = KSPSetFromOptions(kspSOLVER); CHKERRV(ierr);

  // Preconditioner
  PC prec;  
  ierr = KSPGetPC(kspSOLVER,&prec); CHKERRV(ierr);
  ierr = PCSetFromOptions(prec); CHKERRV(ierr);
} 


 // Check convergence
bool PetscData::HasConverged(const double eps,
			     const double resScale, const double dispScale)
{
  PetscScalar resNorm, dispNorm;
  VecNorm(solutionVEC, NORM_2, &dispNorm);
  VecNorm(resVEC, NORM_2, &resNorm);
  printf("residual L2=%.3e\tdisplacement L2=%.3e\n", resNorm, dispNorm);
  if(dispNorm <=eps*dispScale || resNorm<=eps*resScale)
    return true;
  else
    return false;
}


// Set dirichlet bcs
void PetscData::SetDirichletBCs(const std::vector<int> &dirichletrows,
				const std::vector <double> &dirichletvals)
{
  // If the assembly is not complete
  PetscBool flag;
  PetscErrorCode ierr;
  ierr = MatAssembled(stiffnessMAT, &flag); CHKERRV(ierr);
  if(flag==PETSC_FALSE)
    {
      ierr = MatAssemblyBegin(stiffnessMAT,MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = MatAssemblyEnd(stiffnessMAT,MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = VecAssemblyBegin(resVEC); CHKERRV(ierr);
      ierr = VecAssemblyEnd(resVEC); CHKERRV(ierr);
    }

  // Set boundary conditions
  const int nDirichlet = static_cast<int>(dirichletrows.size());
  MatSetOption(stiffnessMAT, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
  MatZeroRows(stiffnessMAT, nDirichlet, &(dirichletrows[0]),
	      1.0, PETSC_NULL, PETSC_NULL);
  
  // Update the residuals using the values of the specified bcs
  double *ResArray;
  ierr = VecGetArray(resVEC, &ResArray); CHKERRV(ierr);
  for(int i=0; i<nDirichlet; ++i)
    ResArray[dirichletrows[i]] = -dirichletvals[i];
  ierr = VecRestoreArray(resVEC, &ResArray); CHKERRV(ierr);
}

// Solve
void PetscData::Solve()
{ 
  // Finish up any assembly
  PetscBool flag;
  PetscErrorCode ierr;
  ierr = MatAssembled(stiffnessMAT, &flag); CHKERRV(ierr);
  if(flag==PETSC_FALSE)
    {
      ierr = MatAssemblyBegin(stiffnessMAT,MAT_FINAL_ASSEMBLY); CHKERRV(ierr);
      ierr = MatAssemblyEnd(stiffnessMAT,MAT_FINAL_ASSEMBLY);  CHKERRV(ierr);
      ierr = VecAssemblyBegin(resVEC); CHKERRV(ierr);
      ierr = VecAssemblyEnd(resVEC); CHKERRV(ierr);
    }
  
  // Initialize solution vector
  ierr = VecSet(solutionVEC, 0.); CHKERRV(ierr);
  ierr = KSPSetOperators(kspSOLVER, stiffnessMAT, stiffnessMAT, SAME_NONZERO_PATTERN);
  CHKERRV(ierr);
  
  // Solve
  ierr = KSPSolve(kspSOLVER, resVEC, solutionVEC); CHKERRV(ierr);

  // How did it go?
  KSPConvergedReason reason;
  ierr = KSPGetConvergedReason(kspSOLVER, &reason); CHKERRV(ierr);
  if(reason<0)
    {
      PetscPrintf(PETSC_COMM_WORLD, "KSPConvergedReason: %d\n", reason);
      assert(false && "\nSolution has diverged.\n");
    }
  return;
}


void PetscData::Destroy()
{
  PetscErrorCode ierr;
  ierr = VecDestroy(&DOFArray); CHKERRV(ierr);
  ierr = VecDestroy(&solutionVEC); CHKERRV(ierr);
  ierr = VecDestroy(&resVEC); CHKERRV(ierr);
  ierr = MatDestroy(&stiffnessMAT); CHKERRV(ierr);
  ierr = KSPDestroy(&kspSOLVER); CHKERRV(ierr);
}
