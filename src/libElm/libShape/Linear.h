/*
 * Linear.h
 * DG++
 *
 * Created by Adrian Lew on 9/4/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef LINEAR
#define LINEAR

#include "Shape.h"

/**
   \brief Set of linear shape functions in spatial_dimension dimensions

   The linear shape functions in spatial_dimension dimensions are precisely the barycentric
   coordinates of a point in the simplex defined by spatial_dimension+1 points.

   Since quadrature points are often expressed in baricentric coordinates, this class was
   implemented to take the spatial_dimension+1 barycentric coordinates of a point as arguments only. 

   Since barycentric coordinates are linearly dependent, the class chooses spatial_dimension coordinates
   as arguments of the shape functions. This choice is made by shuffling the coordinates so that the first 
   spatial_dimension coordinates are the linearly independent ones.

   The possibility of shuffling becomes useful, for example, in tetrahedra. Without shuffling the class
   would have
   \f[
   N_a(\lambda_0,\lambda_1,\lambda_2), \qquad a=0,1,2,3
   \f]
   and the derivatives of the shape function with respect to the same arguments. With a shuffling  of the form
   \f(\lambda_0,\lambda_1,\lambda_2,\lambda_3)\f to \f(\lambda_0,\lambda_1,\lambda_3,\lambda_2)\f it is 
   possible to have
   \f[
   N_a(\lambda_0,\lambda_1,\lambda_3), \qquad a=0,1,2,3.
   \f]
   The derivatives returned here, are clearly different that in the previous case. The reason that this is 
   useful is that these derivatives are precisely the derivatives with respect to the Cartesian coordinates
   of the shape functions in a standard parametric tet ( with vertices at (1,0,0), (0,1,0), (0,0,0), (0,0,1) )
*/

template <int spatial_dimension>
class Linear: public Shape
{
 public:
  //! Constructor \n
  //! \param iMap Shuffle of the barycentric coordinates. iMap[a] returns the position of the original 
  //! a-th barycentric coordinate after shuffling.
  //! If not provided, an identity mapping is assumed iMap[a]=a

  //! \warning No way to know if iMap has the proper length.
  Linear(const int * iMap = 0);
  inline virtual ~Linear(){}
  Linear(const Linear<spatial_dimension> &);
  inline virtual Linear<spatial_dimension>* Clone() const override
  { return new Linear<spatial_dimension>(*this); }
  
  // Accessors/Mutators
  inline int GetNumberOfFunctions() const override
  { return spatial_dimension+1; }

  inline int GetNumberOfVariables() const override
  { return spatial_dimension;   }
  
  // Functionality

  //! @param a shape function number
  //! @param x first spatial_dimension barycentric coordinates of the point
  //! \warning Does not check range for parameter a
  double Val(const int a, const double *x) const override;

  //! @param a shape function number
  //! @param x first spartial_dimension barycentric coordinates of the point
  //! @param i partial derivative number 
  //! Returns derivative with respect to the barycentric coordinates
  //! \warning Does not check range for parameters a and i
  double DVal(const int a, const double *x, int i) const override;

 private:
  int bctMap[spatial_dimension+1];
};



template <int spatial_dimension>
Linear<spatial_dimension>::Linear(const int *iMap)
{
  for(int a=0; a<spatial_dimension+1; ++a)
    bctMap[a] = a;
  
  if(iMap != 0)
    for(int a=0; a<spatial_dimension+1; ++a)
      bctMap[a] = iMap[a];
  
  return;
}



template <int spatial_dimension>
Linear<spatial_dimension>::Linear(const Linear<spatial_dimension> &Lin)
{
  for(int a=0; a<spatial_dimension+1; ++a)
    bctMap[a] = Lin.bctMap[a];
}


template <int spatial_dimension>
double Linear<spatial_dimension>::Val(const int a, const double *x)  const
{
  if(bctMap[a]!=spatial_dimension)
    return x[bctMap[a]];
  else
    {
      double va = 0;
      
      for(int k=0; k<spatial_dimension; ++k)
	va += x[k];
      
      return 1.-va;
    }
}


template <int spatial_dimension>
double Linear<spatial_dimension>::DVal(const int a, const double *x, const int i) const
{
  if(bctMap[a]!=spatial_dimension)
    return bctMap[a]==i? 1.:0.;
  else
    return -1.;
}


#endif
