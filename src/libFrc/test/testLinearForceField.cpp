/*
 * testLinearForceField.cpp
 * DG++
 *
 * Created by Adrian Lew on 09/13/07.
 *  
 * Copyright (c) 2007 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include <vector>
#include <iostream>
#include <cassert>
#include "LinearForceField.h"

int main()
{
  std::vector<double> uGrad(6);
  std::vector<double> XGrad(2);

  uGrad[0] = 1;
  uGrad[1] = 2;
  uGrad[2] = 3;
  uGrad[3] = 4;
  uGrad[4] = 5;
  uGrad[5] = 6;

  XGrad[0] = -1;
  XGrad[1] = -2;

  LinearForceField LFF(uGrad, XGrad, 3, 1);

  assert(LFF.GetULength()==3);
  assert(LFF.GetXLength()==1);
  assert(LFF.GetForceLength()==2);

  std::vector<double> u(3);
  std::vector<double> X(1);
  std::vector<double> ForceVal;

  u[0] = 10;
  u[1] = 20;
  u[2] = 30;
  X[0] = -10;
  LFF.GetForceField(u,X,&ForceVal);
  std::cout << "ForceVal: "<< ForceVal[0] << " " << ForceVal[1] << "\n";

  LinearForceField LFF_Copy(LFF);
  LFF_Copy.GetForceField(u,X,&ForceVal);
  std::cout << "ForceVal: "<< ForceVal[0] << " " << ForceVal[1] << "\n";
  
  assert(LFF.ConsistencyTest(LFF, u, X) && "Consistency failed");
}
