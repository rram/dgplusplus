#ifndef TETRAHEDRON
#define TETRAHEDRON

#include "Triangle.h"
#include <cstdlib>

/**
   \brief Geometry of 3D tetrahedra.
   
   A tetrahedron is:
   1) A set of indices that describe the connectivity of the tetrahedran, properly oriented. 
   
   2) An affine map from a three-dimensional tetrahedron (parametric configuration) with 
   volume 1/6 to the 
   convex hull of 4 vertices. 
   
   The parametric configuration of the tetrahedron is 0(1,0,0), 1(0,1,0), 2(0,0,0), 3(0,0,1).
   The parametric coordinates are the ones associated with vertices 0,1 and 3.
   The faces (for the purpose of quadrature points) are ordered as:
   1) Face 1: 2-1-0,
   2) Face 2: 2-0-3,
   3) Face 3: 2-3-1,
   4) Face 4: 0-1-3.
   The convention used in numbering these faces is that the resulting normal is 
   always outward.

*/

class Tetrahedron: public ElementGeometry
{
 public:
  
  //! Connectivity in Tetrahedron::GlobalCoordinatesArray
  inline Tetrahedron(const int i1, const int i2, 
		     const int i3, const int i4)
  {
    Connectivity.push_back(i1); 
    Connectivity.push_back(i2);  
    Connectivity.push_back(i3);
    Connectivity.push_back(i4);
  }

  inline virtual ~Tetrahedron() {}

  //! Copy constructor
  inline Tetrahedron(const Tetrahedron& T)
  { Connectivity = T.Connectivity; }

  //! Cloning
  inline virtual Tetrahedron* Clone() const override
  { return new Tetrahedron(*this); }
  
  //! Returns the number of vertices.
  inline int GetNumberOfVertices() const override
  { return 4; }
  
  //! Returns the connectivity array
  inline const std::vector<int>& GetConnectivity() const override
  { return Connectivity; }
  
  //! Returns the name of the geometry. It clarifies the meaning of the connectivity array.
  inline std::string GetPolytopeName() const override
  { return "TETRAHEDRON"; }

  //! Returns the number of dimensions in the parametric configuartion.
  inline int GetParametricDimension() const override
  { return 3; }

  //! Returns the number of dimensions in the real configuration.
  inline int GetEmbeddingDimension() const override
  { return 3; }

  //! Number of faces the polytope has.
  inline int GetNumberOfFaces() const override
  { return 4; }
  
  //! Map from parametric to real configuration.
  //! \param X parametric coordinates.
  //! \param Y returned real coordinates.
  void Map(const double *X, double *Y) const override;

  //! Derivative of the map from the parametric to the real configuration.
  //! \param X parametric cooridnates
  //! \param Jac returns Jacobian of the map.
  //! \param DY returnd derivative of the map. 
  //! Here DY[a*GetEmbeddingDimension()+i] contains the derivative of the a-th direction of the i-th coordinate.
  void DMap(const double *X, double *DY, double &Jac) const override;

  //! Creates a new ElementGeometry object corresponding to face 'e' in the polytope. The object ghas to be 
  //! deleted by the recepient.
  //! \param e facenumber , starting from 0.
  //! Prompts an error if an invalid face is requested.
  Triangle<3>* GetFaceGeometry(const int e) const override;

  //! Set the only static member of the class , GlobalCoordinatesArray.
  inline static void SetGlobalCoordinatesArray(const std::vector<double> & coord)
    { GlobalCoordinatesArray = &coord; }
  
 protected:
  //! Interface to avoid explicitly using GlobalCoordinatesArray everywhere, since the latter is likely to 
  //! change later on.
  virtual double GetCoordinate(int a, int i) const
    { return (*GlobalCoordinatesArray)[(GetConnectivity()[a]-1)*3+i]; }


 private:
  static const std::vector<double> * GlobalCoordinatesArray;
  std::vector<int> Connectivity;

};


#endif
