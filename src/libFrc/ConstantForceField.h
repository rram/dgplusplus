/*
 * ConstantForceField.h
 * DG++
 *
 * Created by Adrian Lew on 09/12/07.
 *  
 * Copyright (c) 2007 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef CONSTANTFORCEFIELD
#define CONSTANTFORCEFIELD

#include "ForceField.h"
#include <string>
#include <cstdlib>
#include <cassert>

/** 
    \brief Constant force field per unit volume.

    General expression for the force per unit volume
    \f[
    F({\bf X}) = {\bf g}
    \f]
    where \f${\bf g}\f$ is a vector independent of position or field values. 
    
    The dimensions of \f${\bf g}\f$ and \f${\bf X}\f$ can be independently set.

    It can serve to model gravity force, for example.
*/
class ConstantForceField: public DForceField
{
 public:
  //! @param ForceFieldVal: vector with the components of \f${\bg g}\f$. 
  //! It is copied, not referenced
  inline ConstantForceField(const std::vector<double> & ForceFieldVal): 
  LocalForceField(ForceFieldVal) {}
  
  inline virtual ~ConstantForceField() {}
  
 ConstantForceField(const ConstantForceField & OldCFF):LocalForceField(OldCFF.LocalForceField) {}
  
  virtual ConstantForceField * Clone() const { return new ConstantForceField(*this); }  

  inline bool GetForceField(const std::vector<double> uVal, 
			    const std::vector<double> XVal,
			    std::vector<double> *ForceFieldVal) const
  { return GetDForceField(uVal, XVal, ForceFieldVal, 0); }
  
  inline bool GetDForceField(const std::vector<double> u, 
			     const std::vector<double> X,
			     std::vector<double> *ForceFieldVal,
			     std::vector<double> *DForceFieldVal) const
  { 
    if(ForceFieldVal->size()<GetForceLength()) ForceFieldVal->resize(GetForceLength());
    for(unsigned int i=0; i<GetForceLength(); i++) 
      (*ForceFieldVal)[i] = LocalForceField[i];
    
    return true;
  }

  
  //! Returns the expected lengths of uVal
  inline unsigned int GetULength() const { return 0; }

  //! Returns the expected lengths of XVal
  inline unsigned int GetXLength() const { return 0; }
  
  //! Returns the expected length of the Force vector (output)
  virtual unsigned int GetForceLength() const { return LocalForceField.size();}

  //! Returns the name of the force field
  const std::string GetForceFieldName() const { return "ConstantForceField"; }

  //! Changes the value of the force field
  //! @param fields: vector with the components of \f${\bg g}\f$. 
  //! It is copied, not referenced
  inline void SetForceField(const std::vector<double> &fields)
  {
    assert(fields.size()==GetForceLength() &&
	   "ConstantForceField::SetForceField: Incompatible size of new force field");
    
    for(unsigned int i=0; i<GetForceLength(); ++i)
      LocalForceField[i] = fields[i];
  }

 private:
  std::vector<double> LocalForceField;
};


#endif
