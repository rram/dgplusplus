/*
 * StressWork.cpp
 * DG++
 *
 * Created by Adrian Lew on 10/25/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "StressWork.h"
#include <cmath>

void StressWork::GetDVal(const void* arg,
			 std::vector<std::vector<double>>* funcval,
			 std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
{
  // Read the deformation mapping
  auto* dispptr = static_cast<const VectorMap*>(arg);
  assert(dispptr!=nullptr && "StressWork::GetDVal- Invalid configuration");
  auto& DispMap = *dispptr;
  assert(DispMap.IsInitialized() && "StressWork::GetDVal- Displacement map not initialized");

  // Local node numbers for this element's access to the displacement field
  const auto& LocNodes = VMAccess.Get();
  
  // Zero out the outputs;
  SetZero(funcval, dfuncval);

  int Dim = int(FieldsUsed.size());
  
  // Aliases to be used from the workspace
  auto& A = WrkSpc->A; // Material tangents
  auto& P = WrkSpc->P; // First PK stress
  auto& F = WrkSpc->F; // Deformation gradient
  auto& nDof = WrkSpc->nDof; // Number of dofs per field
  auto& IntWeights = WrkSpc->IntWeights; // Pointer to integration weights
  auto& DShape = WrkSpc->DShape; // Field-wise shape derivatives
    
  // We should have the same quadrature points in  all fields used
  IntWeights = &Elm->GetIntegrationWeights(FieldsUsed[0]);
  const int nquad = static_cast<int>(IntWeights->size());

  // Field-wise dofs and shape function derivatives
  for(int f=0; f<Dim; ++f)
    {
      nDof[f] = Elm->GetDof(FieldsUsed[f]);
      DShape[f] = &Elm->GetDShape(FieldsUsed[f]);
    }

  bool flag; 
  for(int q=0; q<nquad; ++q)
    {
      // Compute gradients
      F[0]=F[4]=F[8]=1.;
      F[1]=F[2]=F[3]=F[5]=F[6]=F[7]=0.;
      
      for(int f=0; f<Dim; ++f)
	for(int a=0; a<nDof[f]; ++a)
	  {
	    const auto* NodalDisp = DispMap.Get(LocNodes[a]);
	    for(int J=0; J<Dim; ++J)
	      F[f*3+J] += NodalDisp[f]* (*DShape[f])[q*nDof[f]*Dim+a*Dim+J];
	  }

      flag = LocalSimpleMaterial->GetConstitutiveResponse( &F, &P, &A);
      assert(flag && "StressWork::GetDVal: Error in the constitutive response");

      for(int f=0; f<Dim; ++f)
	for(int a=0; a<nDof[f]; ++a)
	  for(int J=0; J<Dim; ++J)
	    (*funcval)[f][a] +=  (*IntWeights)[q] *P[f*3+J] * (*DShape[f])[q*nDof[f]*Dim+a*Dim+J];
      
      if(dfuncval!=nullptr)
	for(int f=0; f<Dim; ++f)
	  for(int a=0; a<nDof[f]; ++a)
	    for(int g=0; g<Dim; ++g)
	      for(int b=0; b<nDof[g]; ++b)
		for(int J=0; J<Dim; ++J)
		  for(int L=0; L<Dim; ++L)
		    (*dfuncval)[f][a][g][b] += (*IntWeights)[q]*     
		      A[f*27+J*9+g*3+L] *
		      (*DShape[f])[q*nDof[f]*Dim+a*Dim+J] *
		      (*DShape[g])[q*nDof[g]*Dim+b*Dim+L];    
    }
  
  return;
}
