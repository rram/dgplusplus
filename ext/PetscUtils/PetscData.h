// Sriramajayam

#ifndef PETSC_DATA_STRUCT
#define PETSC_DATA_STRUCT

#include <vector>
#include "petscmat.h"
#include "petscvec.h"
#include "petscksp.h"

struct PetscData
{
  Vec  resVEC;
  Vec  solutionVEC;
  Vec  DOFArray;
  Mat  stiffnessMAT;
  KSP  kspSOLVER;
  
  //! \param[in] nz Number of nonzeros per row.
  void Initialize(const std::vector<int>& nz);

  // Check convergence
  bool HasConverged(const double eps,
		    const double resScale, const double dispScale);

  // Set dirichlet bcs
  void SetDirichletBCs(const std::vector<int> &dirichletrows,
		       const std::vector <double> &forces);

  // Solve
  void Solve();

  // Destroy data structures
  void Destroy();
};


#endif
