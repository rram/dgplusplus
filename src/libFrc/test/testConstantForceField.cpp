/*
 * testConstantForceField.cpp
 * DG++
 *
 * Created by Adrian Lew on 09/13/07.
 *  
 * Copyright (c) 2007 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include <vector>
#include <iostream>
#include "ConstantForceField.h"

int main()
{
  std::vector<double> ff(3);
  ff[0] = 1;
  ff[1] = 2;
  ff[2] = 3;

  ConstantForceField CFF(ff);

  assert(CFF.GetULength()==0);
  assert(CFF.GetXLength()==0);
  assert(CFF.GetForceLength()==3);

  std::vector<double> u;
  std::vector<double> X;
  std::vector<double> ForceVal;
  CFF.GetForceField(u,X,&ForceVal);
  std::cout << "ForceVal: "<< ForceVal[0] << " " << ForceVal[1] << " " << ForceVal[2] << "\n";

  ConstantForceField CFF_Copy(CFF);
  CFF_Copy.GetForceField(u,X,&ForceVal);
  std::cout << "ForceVal: "<< ForceVal[0] << " " << ForceVal[1] << " " << ForceVal[2] << "\n";
  
  ff[0] = 4;
  ff[1] = 5;
  ff[2] = 6;

  CFF.SetForceField(ff);
  CFF.GetForceField(u,X,&ForceVal);
  std::cout << "ForceVal: "<< ForceVal[0] << " " << ForceVal[1] << " " << ForceVal[2] << "\n";
  
  assert(CFF.ConsistencyTest(CFF, u, X) && "Consistency test failed");
}
