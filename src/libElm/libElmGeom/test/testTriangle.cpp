/*
 * testTriangle.cpp
 * DG++
 *
 * Created by Adrian Lew on 9/7/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 


#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Triangle.h"
#include <cassert>


int main()
{
  std::vector<double> dummycoordinates({0,0,0.5,0.5,0.2,1.5});
  Triangle<2> MyTriangle(1,2,3);
  MyTriangle.SetGlobalCoordinatesArray(dummycoordinates);

  assert(MyTriangle.GetNumberOfVertices()==3);
  assert(MyTriangle.GetParametricDimension()==2);
  assert(MyTriangle.GetEmbeddingDimension()==2);
  
  double X[2];
  X[0] = double(rand())/double(RAND_MAX);
  X[1] = double(rand())/double(RAND_MAX); //It may be outside the triangle

  assert(MyTriangle.ConsistencyTest(X,1.e-6));

  // Faces
  ElementGeometry *face = MyTriangle.GetFaceGeometry(2);
  assert(face->GetNumberOfVertices()==2);
  assert(face->GetParametricDimension()==1);
  assert(face->GetEmbeddingDimension()==2);
  const auto& conn = face->GetConnectivity();
  assert(conn[0]==3 && conn[1]==1);
  delete face;

  // Test virtual mechanism and copy  constructors
  ElementGeometry *MyElmGeo = &MyTriangle;
  assert(MyElmGeo->GetPolytopeName()=="TRIANGLE");
  const auto& Conn = MyElmGeo->GetConnectivity();
  assert(Conn[0]==1 && Conn[1]==2 && Conn[2]==3);

  // 3D triangle
  std::vector<double> dummycoordinates3({0,0,0, 0.5,0.3,1, 0.2,1.5,2});
  Triangle<3> MyTriangle3(1,2,3);
  MyTriangle3.SetGlobalCoordinatesArray(dummycoordinates3);
  assert(MyTriangle3.GetNumberOfVertices()==3);
  assert(MyTriangle3.GetParametricDimension()==2);
  assert(MyTriangle3.GetEmbeddingDimension()==3);
  
  X[0] = double(rand())/double(RAND_MAX);
  X[1] = double(rand())/double(RAND_MAX); //It may be outside the triangle
  assert(MyTriangle3.ConsistencyTest(X,1.e-6));
}
