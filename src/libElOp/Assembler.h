// Sriramajayam

#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include "ElementalOperation.h"
#include "LocalToGlobalMap.h"
#include <cassert>
#include <set>
#include "petscmat.h"
#include "petscvec.h"

//! Interface class for assembly
class Assembler
{
 public:
  
  //! Constructor
  inline Assembler() {}

  //! Destructor
  inline virtual ~Assembler() {}

  //! Copy constructor
  //! \param[in] Obj Object to be copied
  Assembler(const Assembler& Obj) {}

  //! Cloning
  virtual Assembler* Clone() const = 0;
  
  //! Main functionality. Assembles the residual.
  //! To be implemented by derived classes/specializations
  //! \param[in] State Configuration at which to assemble (i.e., dofs)
  //! \param[out] ResVec assembled residual vector
  //! \param[in] ZeroFlag If true, zeroes the vector
  virtual void Assemble(const void* State, Vec& ResVec,
			const bool ZeroFlag=true) = 0;

  //! Main functionality. Assembles residual and stiffness
  //! To be implemented by derived classes/specializations
  //! \param[in] State Configuration at which to assemble (i.e., dofs)
  //! \param[out] ResVec assembed residual vector
  //! \param[out] DResMat assembled stiffness matrix
  //! \param[in] ZeroFlag If true, zeroes the vector and matrix
  virtual void Assemble(const void* State, Vec& ResVec, Mat& DResMat,
			const bool ZeroFlag=true) = 0;

  //! Estimates the number of non-zeros for sparsity
  //! \param[out] nz Computed number of nonzeros.
  virtual void CountNonzeros(std::vector<int>& nz) const = 0;
};



//! Assumes that each operation has identical fields/dofs.
template<class T>
class StandardAssembler: public Assembler
{
 public:
  //! Constructor
  //! \param[in] ops Operations for which to assemble. referenced.
  //! \param[in] l2gmap Local to global map, referenced
  StandardAssembler(std::vector<T*>& ops,
		    const LocalToGlobalMap& l2gmap);

  //! Destructor
  inline virtual ~StandardAssembler() {}

  //! Copy constructor
  //! \param[in] Obj Object to be copied
  StandardAssembler(const StandardAssembler<T>& Obj);

  //! Cloning
  inline virtual StandardAssembler<T>* Clone() const override
  { return new StandardAssembler<T>(*this); }
  
  //! Returns the element operations
  inline const std::vector<T>& GetOperations() const
  { return ElOps_; }
  
  //! Return the local to global map
  inline const LocalToGlobalMap& GetLocalToGlobalMap() const
  { return L2GMap_; }

  //! Main functionality. Assembles the residual.
  //! \param[in] State Configuration at which to assemble
  //! \param[out] ResVec assembled residual vector
  //! \param[in] ZeroFlag If true, zeroes the vector
  virtual void Assemble(const void* State, Vec& ResVec,
			const bool ZeroFlag=true) override;

  //! Main functionality. Assembles residual and stiffness
  //! \param[in] State Configuration at which to assemble
  //! \param[out] ResVec assembed residual vector
  //! \param[out] DResMat assembled stiffness matrix
  //! \param[in] ZeroFlag If true, zeroes the vector and matrix
  virtual void Assemble(const void* State, Vec& ResVec, Mat& DResMat,
			const bool ZeroFlag=true) override;

  //! Estimates the number of non-zeros for sparsity
  //! Assumes that all dofs within each element are related to each other.
  //! \param[out] nz Computed number of nonzeros.
  virtual void CountNonzeros(std::vector<int>& nz) const override;

 protected:

  //! Local data structures used during assembly
  struct ElmDataStruct
  {
    //! Helper method to resize vectors
    void Resize(const std::vector<int>& dofs);
    //! Zeroes out elemental vectors
    void ZeroRes();
    //! Zeros out elemental matrices
    void ZeroDRes();
    std::vector<int> indices;
    std::vector<double> resvals, dresvals;
    std::vector<std::vector<double>> funcval;
    std::vector<std::vector<std::vector<std::vector<double>>>> dfuncval;
  };
  
  std::vector<T*>& ElOps_; //!< Reference to operations vector
  const LocalToGlobalMap& L2GMap_; //!< reference to local 2 global map
  std::vector<int> fields_; //!< Fields for this set of operations
  std::vector<int> dofs_; //!< Number of dofs corresponding to each field
  int nDofsPerOp_; //!< Total number of entries per operation.
  ElmDataStruct ElmDs_; //!< Local data structures used in each operation
};


// Implementation of StandardAssembler<T>

template<class T> void StandardAssembler<T>::
ElmDataStruct::Resize(const std::vector<int>& dofs)
{  
  const int nFields = static_cast<int>(dofs.size());
  int nTotalDofs = 0;
  funcval.resize(nFields);
  dfuncval.resize(nFields);
  for(int f=0; f<nFields; ++f)
    {
      funcval[f].resize( dofs[f] );
      dfuncval[f].resize( dofs[f] );
      nTotalDofs += dofs[f];
      
      for(int a=0; a<dofs[f]; ++a)
	{
	  dfuncval[f][a].resize( nFields );
	  for(int g=0; g<nFields; ++g)
	    dfuncval[f][a][g].resize( dofs[g] );
	}
    }
  indices.resize(nTotalDofs);
  resvals.resize(nTotalDofs);
  dresvals.resize(nTotalDofs*nTotalDofs);
  return;
}

// Zeroes out elemental vectors
template<class T> void
StandardAssembler<T>::ElmDataStruct::ZeroRes()
{
  for(auto& v:funcval)
    for(auto& w:v)
      w = 0.;
}

// Zeros out elemental matrices
template<class T> void
StandardAssembler<T>::ElmDataStruct::ZeroDRes()
{
  for(auto& u:dfuncval)
    for(auto& v:u)
      for(auto& w:v)
	for(auto& x:w)
	  x = 0.;
}


// Constructor
template<class T> StandardAssembler<T>::
StandardAssembler(std::vector<T*>& ops,
		  const LocalToGlobalMap& l2gmap)
:ElOps_(ops), L2GMap_(l2gmap)
{
  assert(ElOps_.size()>0 &&
	 "\nStandardAssembler::StandardAssembler()- Operation array is empty.\n");

  // Use the fields/dofs from first operation for data structure allocations.
  const auto* op = ElOps_[0];
  
  // Fields
  fields_ = op->GetField();
  const int nFields = static_cast<int>(fields_.size());

  // Dofs for each field and the total sum
  nDofsPerOp_ = 0;
  dofs_.resize(nFields);
  for(int f=0; f<nFields; ++f)
    {
      dofs_[f] = op->GetFieldDof(fields_[f]);
      nDofsPerOp_ += dofs_[f];
    }

  // Element-level data structues
  ElmDs_.Resize(dofs_);
}


// Copy constructor
template<class T> StandardAssembler<T>::
StandardAssembler(const StandardAssembler<T>& Obj)
:ElOps_(Obj.ElOps_), L2GMap_(Obj.L2GMap_),
  fields_(Obj.fields_), dofs_(Obj.dofs_),
  nDofsPerOp_(Obj.nDofsPerOp_)
{ ElmDs_.Resize(dofs_); }


// Main functionality. Assembles the residual.
template<class T> void
StandardAssembler<T>::Assemble(const void* State, Vec& ResVec,
			       const bool ZeroFlag)
{
  PetscErrorCode ierr;
  
  // Zero the residual vector
  if(ZeroFlag)
    { ierr = VecZeroEntries(ResVec); CHKERRV(ierr); }

  // Number of operations
  const int nOps = static_cast<int>(ElOps_.size());

  // Number of fields
  const int nFields = static_cast<int>(fields_.size());
  
  // Convenient aliases
  auto& funcval = ElmDs_.funcval;
  auto& indices = ElmDs_.indices;
  auto& resvals = ElmDs_.resvals;
  
  // Loop over operations
  for(int e=0; e<nOps; ++e)
    {
      // Make a list of this operation's dofs.
      for(int f=0, i=0; f<nFields; ++f)
	for(int a=0; a<dofs_[f]; ++a, ++i)
	  indices[i] =  L2GMap_.Map(fields_[f], a, e);
      
      // Compute the element residual
      ElOps_[e]->GetVal(State, &funcval);

      // Flatten residue
      for(int f=0, i=0; f<nFields; ++f)
	for(int a=0; a<dofs_[f]; ++a, ++i)
	  resvals[i] = funcval[f][a];

      // Assemble into global residue vector
      ierr = VecSetValues(ResVec, nDofsPerOp_, &indices[0], &resvals[0], ADD_VALUES);
      CHKERRV(ierr);
    }
  
  // Finish up assembly
  VecAssemblyBegin(ResVec);
  VecAssemblyEnd(ResVec);
  
  // Done
  return;
}



// Main functionality. Assembles residual and stiffness
template<class T> void
StandardAssembler<T>::Assemble(const void* State, Vec& ResVec, Mat& DResMat,
			       const bool ZeroFlag)
{
  PetscErrorCode ierr;

  if(ZeroFlag)
    {
      // Zero the entries of the forcing and stiffness
      ierr = VecZeroEntries(ResVec); CHKERRV(ierr);
      ierr = MatZeroEntries(DResMat); CHKERRV(ierr);
    }

  // Number of operations
  const int nOps = static_cast<int>(ElOps_.size());

  // Number of fields
  const int nFields = static_cast<int>(fields_.size());
  
  // Convenient aliases
  auto& funcval = ElmDs_.funcval;
  auto& dfuncval = ElmDs_.dfuncval;
  auto& indices = ElmDs_.indices;
  auto& resvals = ElmDs_.resvals;
  auto& dresvals = ElmDs_.dresvals;
  
  
  // Loop over operations
  for(int e=0; e<nOps; ++e)
    {
      // Make a list of dofs for this operation
      for(int f=0, i=0; f<nFields; ++f)
	for(int a=0; a<dofs_[f]; ++a, ++i)
	  indices[i] = L2GMap_.Map(fields_[f], a, e); 
      
      // Compute the element residue and stiffness
      ElOps_[e]->GetDVal(State, &funcval, &dfuncval);

      // Flatten residue and stiffness
      for(int f=0, i=0, j=0; f<nFields; ++f)
	for(int a=0; a<dofs_[f]; ++a, ++i)
	  {
	    resvals[i] = funcval[f][a];
	    for(int g=0; g<nFields; ++g)
	      for(int b=0; b<dofs_[g]; ++b, ++j)
		dresvals[j] = dfuncval[f][a][g][b];
	  }

      // Assemble residue into global vector
      ierr = VecSetValues(ResVec, nDofsPerOp_, &indices[0], &resvals[0], ADD_VALUES);
      CHKERRV(ierr);

      // Assemble stiffness into global matrix
      ierr = MatSetValues(DResMat, nDofsPerOp_, &indices[0], nDofsPerOp_, &indices[0],
			  &dresvals[0], ADD_VALUES);
      CHKERRV(ierr);
    }

  // Finish up assembly
  VecAssemblyBegin(ResVec);
  VecAssemblyEnd(ResVec);
  MatAssemblyBegin(DResMat, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(DResMat, MAT_FINAL_ASSEMBLY);
  
  // Done
  return;
}


// Count number of nonzeros
template<class T> void
StandardAssembler<T>::CountNonzeros(std::vector<int>& nz) const
{
  // We need the number of nonzeros.
  // Count relations between degrees of freedom
  // Assume that all degrees of freedom in one element are related
  const int ndof = L2GMap_.GetTotalNumDof();
  nz.resize(ndof);
  std::fill(nz.begin(), nz.end(), 0);
  
  const int nElements = static_cast<int>(L2GMap_.GetNumElements());
  std::vector<std::set<int>> dofrelations(ndof);
  int DofPerField;
  for(int e=0; e<nElements; ++e)
    {
      // Visit each dof within this element
      std::set<int> ElmDofs;
      const int nFields = L2GMap_.GetNumFields(e);
      for(int f=0; f<nFields; ++f)
	{
	  DofPerField = L2GMap_.GetNumDofs(e,f);
	  for(int a=0; a<DofPerField; ++a)
	    ElmDofs.insert( L2GMap_.Map(f,a,e) );
	}
	
      for(auto& itA:ElmDofs)
	for(auto& itB:ElmDofs)
	  dofrelations[itA].insert( itB );
    }

  // Count the number of nonzeros.
  for(int i=0; i<ndof; ++i)
    nz[i] = static_cast<int>(dofrelations[i].size());
}
  

#endif
