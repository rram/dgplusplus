/*
 * ShapesEvaluatedImpl.cpp
 * DG++
 *
 * Created by Adrian Lew on 9/7/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "ShapesEvaluated.h"
#include <cassert>


extern "C" void dgesv_(int *, int *, double *, int *, int *, double *, int *,
		       int *);

void ShapesEvaluated::CreateObject(const Quadrature* TQuadrature,
				   const Shape* TShape, 
				   const ElementGeometry* EG)
{
  const int Na = TShape->GetNumberOfFunctions();
  const int Nq = TQuadrature->GetNumberQuadraturePoints();
  const int Nd = EG->GetEmbeddingDimension();
  int Np = EG->GetParametricDimension();

  LocalShapes.resize(Nq*Na);
  if(Np==Nd)  LocalDShapes.resize(Nq*Na*Nd);
  LocalWeights.resize(Nq);
  LocalCoordinates.resize(Nq*Nd);
  
  double *DY = new double[Nd*Np];
  double *Y  = new double[Nd];
  
  for(int q=0; q<Nq; ++q)
    {
      EG->Map(TQuadrature->GetQuadraturePoint(q), Y);
      for(int i=0; i<Nd ; i++) 
	LocalCoordinates[q*Nd+i] = Y[i];
      
      for(int a=0; a<Na ; a++)
	LocalShapes[q*Na+a] = TShape->Val(a, TQuadrature->GetQuadraturePointShape(q));

      double Jac;
      EG->DMap(TQuadrature->GetQuadraturePoint(q), DY, Jac);

      LocalWeights[q] = (TQuadrature->GetQuadratureWeights(q))*Jac;

      // Compute derivatives of shape functions only when the element map goes
      // between spaces of the same dimension
      if(Np==Nd)
	{
	  double *DYInv = new double[Np*Np];
	  double *DYT   = new double[Np*Np];
            
	  // Lapack
	  {
	    // Transpose DY to Fortran mode
	    for(int k=0; k<Nd; k++)
	      for(int M=0; M<Np; M++)
		{
		  DYT[k*Nd+M] = DY[M*Np+k];

		  //Right hand-side
		  DYInv[k*Nd+M] = k==M?1:0; 
		}

	    int    *IPIV = new int[Np];
	    int    INFO;

	    // DYInv contains the transpose of the inverse
	    dgesv_(&Np, &Np, DYT, &Np, IPIV, DYInv, &Np, &INFO);
	    
	    assert( (INFO==0) &&
		    "\nShapesEvaluated::CreateObject: Lapack could not invert matrix\n" );
	    
	    delete[] DYT;
	    delete[] IPIV;
	  }
	  
	  for(int a=0; a<Na; ++a)
	    for(int i=0; i<Nd; ++i) 
	      {
		LocalDShapes[q*Na*Nd+a*Nd+i] = 0;
		for(int M=0; M<Np; ++M)
		  LocalDShapes[q*Na*Nd+a*Nd+i] += 
		    TShape->DVal(a, TQuadrature->GetQuadraturePointShape(q), M)
		    *DYInv[M*Np+i];
	      }
	  
	  delete[] DYInv;
	}
    }
  delete[] DY;
  delete[] Y;
}
