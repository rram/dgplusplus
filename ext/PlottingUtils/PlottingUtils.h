// Sriramajayam

#ifndef PLOTTINGUTILS
#define PLOTTINGUTILS

#include "External.h"

//! Plots a CoordConn mesh in tecplot format.
//! This is done by simply calling PlotTecCoordConnWithNodalFields() without any nodal fields.
//! \param filename Name of file to plot in tecplot format
//! \param MD CoordConn object. Should have element type TRIANGLE, TETRAHEDRON, QUADRILATERAL or BRICK
//! \param ET Element type
void PlotTecCoordConn(const char * filename, const CoordConn &MD, const char*ET=NULL);


//! Plots a CoordConn mesh along with nodal fields in tecplot format
//! \param filename Name of file to plot in tecplot format
//! \param MD CoordConn object. Should have element type TRIANGLE, TETRAHEDRON, QUADRILATERAL or BRICK
//! \param nodalfieldtoplot Nodal field to be plotted. Defaulted to NULL
//! \param nFields Number of nodal fields. Defaulted to 0.
//! \param ET Element type
void PlotTecCoordConnWithNodalFields(const char * filename, const CoordConn &MD, const double * nodalfieldtoplot=0, const int nFields=0, const char*ET=NULL);



//! Plots a given set of elements of a CoordConn mesh in tecplot format
//! \param filename Name of file to plot in tecplot format
//! \param MD CoordConn object. Should have element type TRIANGLE, TETRAHEDRON, QUADRILATERAL
//! \param ElmList Vector of element numbers. Not checked for repetitions
void PlotTecCoordConnElmList(const char * filename, const CoordConn &MD, const std::vector<int> &ElmList);

#endif
