/*
 * testSegment.cpp
 * DG++
 *
 * Created by Adrian Lew on 10/8/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 


#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Segment.h"
#include <cassert>

int main()
{
  // Fill-in the dummy global array
  std::vector<double> dummycoordinates({0.,0.,0.5,0.3});
  
  Segment<2> MySegment(1,2);
  MySegment.SetGlobalCoordinatesArray(dummycoordinates);

  assert(MySegment.GetNumberOfVertices()==2);
  assert(MySegment.GetParametricDimension()==1);
  assert(MySegment.GetEmbeddingDimension()==2);

  double X[2];
  X[0] = double(rand())/double(RAND_MAX);  //It may be outside the segment

  assert(MySegment.ConsistencyTest(X,1.e-6));
	 
  // Test virtual mechanism and copy constructors
  ElementGeometry *MyElmGeo = &MySegment;
  assert(MyElmGeo->GetPolytopeName()=="SEGMENT");
  const auto& Conn = MyElmGeo->GetConnectivity();
  assert(Conn[0]==1 && Conn[1]==2);
  

  // 3D segment
  std::vector<double> dummycoordinates3({0.,0.,0.,0.5,0.3,1.});  
  Segment<3> MySegment3(1,2);
  MySegment3.SetGlobalCoordinatesArray(dummycoordinates3);
  assert(MySegment3.GetNumberOfVertices()==2);
  assert(MySegment3.GetParametricDimension()==1);
  assert(MySegment3.GetEmbeddingDimension()==3);
  
  X[0] = double(rand())/double(RAND_MAX); // It may be outside the segment
  assert(MySegment3.ConsistencyTest(X,1.e-6));
}
