// Sriramajayam

/*
 * MeshUtils.h
 * DG++
 *
 * Created by Ramsharan Rangarajan on 08/20/2010.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 


#ifndef MESHUTILS
#define MESHUTILS

#include <iostream>
#include "External.h"
#include <vector>
#include <string>
#include <set>

//! Function to read a mesh and nodal fields from a file in tecplot format.
//! The mesh is assumed to have the same element type which has to be one of TRIANGLE, 
//! TETRAHEDRON or QUADRILATERAL.
//! \param filename Name of file to read
//! \param MD Name of mesh to read. Should have spatial_dimension and nodes_element pre-set
//! \param nFields Number of nodal fields to read. Default is set to 0.
//! Returns a vector of nodal fields
std::vector<double> ReadTecplotFile(const std::string, CoordConn &MD, int nFields=0);




//! Function to return the face neighbor list in a mesh of triangles in 2D or tetrahedra in 3D.
//! \param connectivity Connectivity of mesh object
//! \param nodes_element Number of nodes per element
//! \param spatial_dimension Spatial dimension for mesh
//! \param ElmNeighbors Output. Neighbor list. 
//! ElmNeighbors[e][2*f] gives the neighboring element number and 
//! ElmNeighbors[e][2*f+1] gives the neighbors face number.
void GetCoordConnFaceNeighborList(const std::vector<int> &connectivity, 
				  const int nodes_element, 
				  std::vector< std::vector<int> > &ElmNeighbors);

//! Function to return the face neighbor list in a mesh of triangles in 2D or tetrahedra in 3D.
//! \param MD Triangle/Tetrahedral mesh object
//! \param ElmNeighbors Output. Neighbor list. 
//! ElmNeighbors[e][2*f] gives the neighboring element number and 
//! ElmNeighbors[e][2*f+1] gives the neighbors face number.
 void GetCoordConnFaceNeighborList(const CoordConn &MD, 
				   std::vector<std::vector<int>> &ElmNeighbors);
//{ return GetCoordConnFaceNeighborList(MD.connectivity, MD.nodes_element, ElmNeighbors); }


#endif
