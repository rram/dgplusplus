/*
 * testLinearSE.cpp
 * DG++
 *
 * Created by Adrian Lew on 9/9/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "Triangle.h"
#include "ShapesEvaluated.h"
#include "BasisFunctionsProvided.h"
#include "Linear.h"
#include <iostream>

static void PrintData(const BasisFunctions& P10Shapes);

int main()
{
  std::vector<double> Vertices0({1,0,0,1,0,0});
  Triangle<2>::SetGlobalCoordinatesArray(Vertices0);
  Triangle<2> P10(1,2,3);
  Linear<2> LinShp;
  const auto QuadObj = Triangle_1::Bulk;
  
  ShapesEvaluated* P10Shapes = new ShapesEvaluated(QuadObj, &LinShp, &P10);
  
  
  BasisFunctionsProvided  BFS(P10Shapes->GetShapes(), P10Shapes->GetDShapes(), 
			      P10Shapes->GetIntegrationWeights(), P10Shapes->GetQuadraturePointCoordinates());
  
  BasisFunctionsProvidedExternalQuad  BFSExternalQuad(P10Shapes->GetShapes(), P10Shapes->GetDShapes(), 
						      P10Shapes->GetIntegrationWeights(), 
						      P10Shapes->GetQuadraturePointCoordinates());

  BasisFunctionsProvidedExternalQuad
    BFSExternalQuadNoShapes(2,
			    P10Shapes->GetDShapes(), 
			    P10Shapes->GetIntegrationWeights(), 
			    P10Shapes->GetQuadraturePointCoordinates());
  
  std::cout << "\nPrint data before deleting original shapes\n";
  std::cout << "\n\nTest BasisFunctionProvided\n";
  PrintData(BFS);

  std::cout << "\n\nTest BasisFunctionProvidedExternalQuad\n";
  PrintData(BFSExternalQuad);

  std::cout << "\n\nTest BasisFunctionProvidedExternalQuadNoShapes\n";
  PrintData(BFSExternalQuadNoShapes);
  
  std::cout << "\n\nTest Copy constructors\n";
  BasisFunctionsProvided BFSCopy(BFS);
  BasisFunctionsProvidedExternalQuad BFSExternalQuadCopy(BFSExternalQuad);

  std::cout << "\n\nTest BasisFunctionProvided\n";
  PrintData(BFSCopy);
  
  std::cout << "\n\nTest BasisFunctionProvidedExternalQuad\n";
  PrintData(BFSExternalQuadCopy);
  

  std::cout << "\n\nTest cloning\n";
  BasisFunctions * BFSClone = BFS.Clone();
  BasisFunctions * BFSExternalQuadClone = BFSExternalQuad.Clone();
  
  std::cout << "\n\nTest BasisFunctionProvided\n";
  PrintData(*BFSClone);
  
  std::cout << "\n\nTest BasisFunctionProvidedExternalQuad\n";
  PrintData(*BFSExternalQuadClone);
  
  delete BFSClone;
  delete BFSExternalQuadClone;
  
  delete P10Shapes;
  
  std::cout << "\nPrint data  after deleted original shapes \n";
  PrintData(BFS);  
}



static void PrintData(const BasisFunctions & P10Shapes)
{
  std::cout << "Function values\n";
  for(unsigned int a=0; a<P10Shapes.GetShapes().size(); a++)
    std::cout <<  P10Shapes.GetShapes()[a] << " ";
  std::cout << "\n";
  
  std::cout << "Function derivative values\n";
  for(unsigned int a=0; a<P10Shapes.GetDShapes().size(); a++)
    std::cout <<  P10Shapes.GetDShapes()[a] << " ";
  std::cout << "\n";
  
  std::cout << "Integration weights\n";
  for(unsigned int a=0; a<P10Shapes.GetIntegrationWeights().size(); a++)
    std::cout <<  P10Shapes.GetIntegrationWeights()[a] << " ";
  std::cout << "\n";
  
  std::cout << "Quadrature point coordinates\n";
  for(unsigned int a=0; a<P10Shapes.GetQuadraturePointCoordinates().size(); a++)
    std::cout <<  P10Shapes.GetQuadraturePointCoordinates()[a] << " ";
  std::cout << "\n";
}
