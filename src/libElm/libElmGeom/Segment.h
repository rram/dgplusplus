/*
 * Segment.h
 * DG++
 *
 * Created by Adrian Lew on 10/7/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef SEGMENT
#define SEGMENT

#include "ElementGeometry.h"
#include <cmath>
#include <cassert>

/**
   \brief Segment: Geometry of straight segments 
   
   A Segment is:\n
   1) A set of indices that describe the connectivity of the segment, 
   properly oriented. The coordinates
   are not stored in the element but wherever the application decides\n
   2) An affine map from a one-dimensional segment (parametric configuration) 
   with length 1 to the convex  
   hull of the two vertices. Segments embedded in two- and three-dimensional space 
   are hence easily handled. \n
   
   The parametric configuration is the segment (0,1).\n
   The parametric coordinate used is the distance to 0.

   \warning Neither Map nor DMap check for bounds of
   their array arguments

   \todo Segment and Triangle are too similar in code. Perhaps it would be good to
   generate and intermediate class that has all the common code.


*/


template<int spatial_dimension> 
class Segment: public ElementGeometry
{
 public:
  //! Connectivity in Segment<spatial_dimension> GlobalCoordinatesArray
  inline Segment(const int i1, const int i2)
  {
    Connectivity.push_back(i1);
    Connectivity.push_back(i2);
  }
  
  inline virtual ~Segment(){}

  inline Segment(const Segment& S)
  { Connectivity = S.Connectivity; }

  inline virtual Segment<spatial_dimension>* Clone() const override
  { return new Segment<spatial_dimension>(*this); }
  
  inline int GetNumberOfVertices() const override
  { return 2; }

  inline const std::vector<int>& GetConnectivity() const override
  { return Connectivity; }

  inline std::string GetPolytopeName() const override
  { return "SEGMENT"; }

  inline int GetParametricDimension() const override
  { return 1; }

  inline int GetEmbeddingDimension() const override
  { return spatial_dimension; }

  //! @param X first parametric coordinate
  //! @param Y Output the result of the map
  void Map(const double * X, double *Y) const override;

  //! @param X first parametric coordinate
  //! @param DY Output the derivative of the map
  //! @param Jac Output the jacobian of the map
  void DMap(const double * X, double *DY, double &Jac) const override; 

  inline int GetNumberOfFaces() const override
  { return 2; }
  
  //! \warning not implemented
  ElementGeometry* GetFaceGeometry(const int e) const override
  { assert(false && "\nSegment<spatial_dimension>::GetFaceGeometry()- ot implemented!\n\n");
    return nullptr;
  }
  
  //! Set the only static member of the class GlobalCoordinateArray
  //! @param coord pointer to the array of all vertex coordinates
  inline static void SetGlobalCoordinatesArray(const std::vector<double> & coord) 
    { GlobalCoordinatesArray = &coord; }

 protected:
  //! Interface to avoid explicitly using GlobalCoordinatesArray everywhere, 
  //! since the latter is likely 
  //! to change later on
  //! @param a local node number
  //! @param i coordinate number
  virtual double  GetCoordinate(const int a, const int i) const
    { return (*GlobalCoordinatesArray)
	[(GetConnectivity()[a]-1)*spatial_dimension+i]; }
  
 private:
  static const std::vector<double> *GlobalCoordinatesArray;  //<! Pointer to the global coordinates array
  std::vector<int> Connectivity;
  
};

// Class implementation
template<int spatial_dimension> 
const std::vector<double>* Segment<spatial_dimension>::GlobalCoordinatesArray = nullptr;



template<int spatial_dimension> 
void Segment<spatial_dimension>::Map(const double* X, double* Y) const
{
  for(int i=0; i<spatial_dimension; ++i)
    Y[i] = X[0]*GetCoordinate(0,i) + (1.-X[0])*GetCoordinate(1,i);
  
  return;
}



template<int spatial_dimension> 
void Segment<spatial_dimension>::DMap(const double* X, double* DY, double& Jac) const
{
  for(int i=0; i<spatial_dimension; ++i)
    DY[i] =  GetCoordinate(0,i) - GetCoordinate(1,i);
  
  double g11=0.;

  for(int i=0; i<spatial_dimension; ++i)
    g11 += DY[i]*DY[i];
  
  Jac=sqrt(g11);
  
  return;
}

#endif
