/*
 * testmeshP13D.cpp
 * DG++
 *
 * Created by Ramsharan Rangarjan on 10/25/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 


#include "P13DElement.h"
#include "StressWork.h"
#include <Assembler.h>
#include <MeshUtils.h>
#include <PetscData.h>
#include <PlottingUtils.h>
#include <cstdio>

// Dirichlet BCs.
void GetDirichletBCs(const std::vector<Element *> &EA,
		     const LocalToGlobalMap & L2GMap,
		     std::vector<int> & boundary,
		     std::vector<double> & bvalues);

int main(int argc, char **argv)
{
  PetscInitialize(&argc,&argv,PETSC_NULL, PETSC_NULL);
  constexpr int SPD = 3;
  
  // Read Femap info:
  CoordConn CC;
  CC.spatial_dimension = SPD;
  CC.nodes_element = 4;
  ReadTecplotFile((char*)"cube.msh", CC);
  PlotTecCoordConn((char*)"refConfig.tec", CC);
  
  // Subdivide as specified:
  int ndiv = 1;
  PetscOptionsGetInt(PETSC_NULL,"-ndiv", &ndiv, PETSC_NULL);
  for(int i=0; i<ndiv; i++)
    SubdivideTetrahedra(CC.connectivity, CC.coordinates, CC.nodes, CC.elements);
  
  // Set the global coordinates array
  Tetrahedron::SetGlobalCoordinatesArray(CC.coordinates);
  Triangle<SPD>::SetGlobalCoordinatesArray(CC.coordinates);
  Segment<SPD>::SetGlobalCoordinatesArray(CC.coordinates);

  // Create elements
  std::vector<Element*> ElementArray(CC.elements);
  for(int e=0; e<CC.elements; e++)
    ElementArray[e] = new P13DElement<SPD>(CC.connectivity[e*4],CC.connectivity[e*4+1],
					   CC.connectivity[e*4+2],CC.connectivity[e*4+3]);
  

  // Local to global map
  StandardP13DMap L2GMap(ElementArray);

  // Displacement field, initialize all components to zero
  VectorMap DispMap(CC.nodes, SPD);
  double zero[SPD] = {0.,0.,0.};
  for(int a=0; a<CC.nodes; ++a)
    DispMap.Set(a, zero);
  DispMap.SetInitialized();

  // Create operations
  std::vector<StressWork*> OperationsArray(CC.elements);
  IsotropicLinearElastic ILE(1.0,1.0);  // Linear material
  Workspace WrkSpc(SPD);
  std::vector<int> Fields({0,1,2});
  for(int e=0; e<CC.elements; e++)
    {
      VectorMapAccess VMAccess(std::vector<int>({CC.connectivity[e*4]-1,CC.connectivity[e*4+1]-1,
	      CC.connectivity[e*4+2]-1,CC.connectivity[e*4+3]-1}));
      OperationsArray[e] = new StressWork(ElementArray[e], ILE, Fields, VMAccess, WrkSpc);
    }
  
  // Assembler
  StandardAssembler<StressWork> Asm(OperationsArray, L2GMap);
  std::vector<int> nz;
  Asm.CountNonzeros(nz);
  
  // Initialize petsc data
  PetscErrorCode ierr;
  PetscData PD;
  PD.Initialize(nz);

  // Set up boundary conditions
  std::vector<double> bvalues;
  std::vector<int> boundary;
  GetDirichletBCs(ElementArray, L2GMap, boundary, bvalues);
  
  // Set prescribed displacement values
  const int nbcs = static_cast<int>(bvalues.size());
  std::set<int> bcset;
  for(int i=0; i<nbcs; ++i)
    if(bcset.find(boundary[i])==bcset.end())
      {
	int node = boundary[i]/SPD;
	int dir = boundary[i]%SPD;
	double val[] = {0.,0.,0.};
	val[dir] = bvalues[i];
	DispMap.Increment(node, val);
	bcset.insert(boundary[i]);
      }
  DispMap.SetInitialized();
  
  // Values to be set for increments in displacements
  std::fill(bvalues.begin(), bvalues.end(), 0.);
  
  
  // Start Newton Raphson
  VecZeroEntries(PD.solutionVEC);
  while(true)
    {
      // Update displacement map
      double* deltaDisp;
      ierr = VecScale(PD.solutionVEC, -1.); CHKERRQ(ierr);
      ierr = VecGetArray(PD.solutionVEC, &deltaDisp); CHKERRQ(ierr);
      for(int a=0; a<CC.nodes; ++a)
	DispMap.Increment(a, &deltaDisp[SPD*a]);
      DispMap.SetInitialized();
      ierr = VecRestoreArray(PD.solutionVEC, &deltaDisp); CHKERRQ(ierr);

      // New update
      Asm.Assemble(&DispMap, PD.resVEC, PD.stiffnessMAT);
      PD.SetDirichletBCs(boundary, bvalues);
      PD.Solve();
      if(PD.HasConverged(1.e-10, 1, 1.))
	{
	  // Plot
	  CoordConn DefMD = CC;
	  const auto& disp = DispMap.Get();
	  for(int i=0; i<CC.nodes*SPD; ++i)
	    DefMD.coordinates[i] += disp[i];
	  PlotTecCoordConn((char*)"defConfig.tec", DefMD);
	  break;
	}
      std::fflush( stdout );
    } // End newton raphson

    // Clean up
  for(int e=0; e<CC.elements; ++e)
    {
      delete ElementArray[e];
      delete OperationsArray[e];
    }
  PD.Destroy();
  PetscFinalize();
}


// Values for Dirichlet BCs:
bool BCValue(double * X, double * bvals)
{
  const double EPS = 1.e-4;
  // Fix bottom
  if(std::abs(X[2]-0.)<=EPS)
    {
      bvals[0] = bvals[1] = bvals[2] = 0.;
      return true;
    }
  // Move top
  else if(std::abs(X[2]-1.)<=EPS)
    {
      bvals[0] = bvals[1] = 0.;
      bvals[2] = 0.5;
      return true;
    }
  else
    return false;
}


// Dirichlet BCs:
void GetDirichletBCs(const std::vector<Element*> &EA,
		     const LocalToGlobalMap & L2GMap,
		     std::vector<int> & boundary,
		     std::vector<double> & bvalues)
{
  double Param[] = {1,0,0,
		    0,1,0,
		    0,0,0,
		    0,0,1};
  
  for(unsigned int e=0; e<EA.size(); e++)
    {
      double Coord[3];
      for(int a=0; a<4; a++)
	{
	  EA[e]->GetElementGeometry().Map(Param+3*a,Coord);
	  double bvals[3];
	  if(BCValue(Coord, bvals))
	    for(int f=0; f<3; f++)
	      {
		boundary.push_back( L2GMap.Map(f,a,e) );
		bvalues.push_back( bvals[f] );
	      }
	}
    }
}
