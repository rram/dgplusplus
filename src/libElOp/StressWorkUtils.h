// Sriramajayam

#ifndef STRESS_WORK_UTILS_H
#define STRESS_WORK_UTILS_H

#include <vector>

//! Helper class to encapsulate the state of a system, i.e., the configuration
//! at which to evaluate operations
class VectorMap
{
 public:
  //! \param[in] nnodes Number of nodes
  //! \param[in] ndim Number of components of vector
  inline VectorMap(const int nnodes, const int ndim)
    :nDim(ndim), Disp(ndim*nnodes), isInitialized(false) {}
  
  //! Destructor, does nothing
  inline virtual ~VectorMap() {}
  
  //! Copy constructor
  inline VectorMap(const VectorMap& obj)
    :nDim(obj.nDim), Disp(obj.Disp), isInitialized(obj.isInitialized) {}

  //! Disable assignment operator
  VectorMap& operator=(const VectorMap&) = delete;

  //! Returns the number of components
  inline int GetNumComponents() const 
  { return nDim; }

  //! Returns the total number of nodes
  inline int GetNumNodes() const
  { return static_cast<int>(Disp.size())/nDim; }
  
  //! Returns the deformation mapping
  inline const std::vector<double>& Get() const
  { return Disp;}

  //! Returns the displacement field at a node
  //! \param[in] a node number
  inline const double* Get(const int node) const
  { return &Disp[nDim*node]; }

  //! Set the value of the displacement at a node
  inline void Set(const int node, const double* val)
  {
    for(int f=0; f<nDim; ++f)
      Disp[nDim*node+f] = val[f];
    isInitialized = false;
  }
  
  //! Increment the value of the deformation map at a node
  inline void Increment(const int node, const double* disp)
  {
    for(int f=0; f<nDim; ++f)
      Disp[nDim*node+f] += disp[f];
    isInitialized = false;
  }

  //! States whether the deformation map has been initialized
  inline bool IsInitialized() const
  { return isInitialized; }

  //! Set as initialized
  inline void SetInitialized()
  { isInitialized = true; }
  
 private:
  const int nDim; //!< Spatial dimension
  std::vector<double> Disp; //!< Vector mapping
  bool isInitialized; //!< Is the deformation map up to date?
};


//! Class to facilitate accessing VectorMap
class VectorMapAccess
{
 public:
 VectorMapAccess(const std::vector<int> n)
    :locnodes(n) {}
  inline VectorMapAccess(const VectorMapAccess& obj)
    :locnodes(obj.locnodes) {}
  inline ~VectorMapAccess() {}
  inline VectorMapAccess& operator=(const VectorMapAccess& rhs)
    {
      if(&rhs==this) return *this;
      locnodes = rhs.locnodes; return *this;
    }
  inline const std::vector<int>& Get() const
  { return locnodes; }
  inline int GetNumNodes() const
  { return static_cast<int>(locnodes.size()); }
 private:
  std::vector<int> locnodes;
};



//! Workspace for element-level intermediate calculations
class Workspace
{
 public:
  const int NFields; //!< Number of fields
  std::vector<double> A; //! Material tangents
  std::vector<double> P; //! First PK stress
  std::vector<double> F; //! Deformation gradient
  std::vector<int> nDof; //!< Number of dofs per field
  std::vector<const std::vector<double>*> DShape; //!< Pointers to field-wise derivatives
  const std::vector<double>* IntWeights; //!< Pointer to integration weights

  //! Constructor
  //! \param[in] nfields Number of fields
  inline Workspace(const int nfields)
    :NFields(nfields), A(81), P(9), F(9),
    nDof(nfields), DShape(nfields) {}
};


#endif
