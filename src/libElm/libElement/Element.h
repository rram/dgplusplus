/*
 * Element.h
 * DG++
 *
 * Created by Adrian Lew on 9/2/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef ELEMENT
#define ELEMENT

#include <vector>
#include "ElementGeometry.h"
#include "BasisFunctions.h"

/**
   \brief Element: abstract base class for any element

   An Element is a convex polytope with possibly-multiple discrete
   functional spaces, one for each field, with support on it.

   An element has:\n
          1) A geometry. The connectivity of vertices that define the convex hull.\n
	  2) A group of scalar functional spaces. Each functional
	     space is defined by a set of basis functions with support
	     on the element. Each functional space has an associated
	     number of degrees of freedom to it: the components of any function
	     in the space in the chosen basis.\n
   
   A field is a scalar-valued function defined over the element.\n
   Each field may have a different underlying functional space.\n
   Each field may have a different quadrature rule. Different quadrature rules
   will also be handled with different elements, or through inheritance. The 
   consistency of the quadrature rule when different fields are integrated together
   is in principle not checked by the element hierarchy.
   \n
   Clearly, isoparametric elements will not necessarily be convex, 
   but Element class can still be used.

   As a convention, fields are numbered starting from 0.

   \todo It would be nice to have vectors and tensors as fields, where for each 
   component one assigns a single set of shape functions and quadrature points. 
   The current abstraction is flexible in the sense that it does not enforce
   vector or tensor fields to have the same quadrature and shapes.

   \todo Need to explain what the expected order of the vectors GetShape() and
   GetDShape() is.

   \todo Need to explain that either GetShape or GetDShape may return an empty vector, 
   signaling that those values are not available. 
*/

class Element
{
 public:

  //! Default constructor
  inline Element(){}

  //! Destructor.
  //! Deletes local shape functions for this element
  inline virtual ~Element()
  {
    for(unsigned int i=0; i<LocalShapes_.size(); i++)
      delete LocalShapes_[i];
  }

  //! Copy constructor
  //! \param[in] Obj Object to be copied from 
  inline Element(const Element &Obj)
    :Fields_(Obj.Fields_)
  {
    for(unsigned int i=0; i<Obj.LocalShapes_.size(); i++)
      LocalShapes_.push_back(Obj.LocalShapes_[i]->Clone());
  }

  //! Cloning
  virtual Element* Clone() const = 0;
  
  //! Returns the number of fields in this element
  virtual int GetNumFields() const
  { return Fields_.size(); }
  
  //! Returns the  fields in this element
  virtual const std::vector<int>& GetFields() const
  { return Fields_; }
  
  //! Number of degrees of freedom of one of the fields.
  //! \param[in] field Field number
  const int GetDof(int field) const
  { return LocalShapes_[GetFieldIndex(field)]->GetBasisDimension(); }
  
  //! Number of derivatives per shape function for one of the fields
  //! \param[in] field field number
  const int GetNumDerivatives(int field) const
  { return LocalShapes_[GetFieldIndex(field)]->GetNumberOfDerivativesPerFunction(); }
  
  //!  Shape functions at quadrature points of one of the fields
  //! \param[in] field  field number
  const std::vector<double>& GetShape(int field) const
  { return LocalShapes_[GetFieldIndex(field)]->GetShapes(); }

  //!  Shape function derivatives at quadrature points of one of the fields
  //! \param[in] field field number
  const std::vector <double> &GetDShape(int field) const
  { return LocalShapes_[GetFieldIndex(field)]->GetDShapes(); }

  //! Integration weights of a given field
  //! \param[in] field field number
  const std::vector<double>& GetIntegrationWeights(int field) const
  { return LocalShapes_[GetFieldIndex(field)]->GetIntegrationWeights(); }

  //! Integration point coordinates of a given field
  //! \param[in] field Field number
  const std::vector<double>& GetIntegrationPointCoordinates(int field) const
  { return LocalShapes_[GetFieldIndex(field)]->GetQuadraturePointCoordinates(); }

  //! Value of shape function for a field at a specific quadrature point
  //! \param[in] f field number
  //! \param[in] q quadrature point number
  //! \param[in] a Shape function number
  const double GetShape(const int f, const int q, const int a) const
  { return LocalShapes_[GetFieldIndex(f)]->GetShapes()[q*GetDof(f)+a]; }
  
  //! Value of derivative of shape function 'a' for field 'f'
  //! at quadrature point 'q' along the coordinate direction 'i'
  //! \param[in] f field number
  //! \param[in] q quadrature point number
  //! \param[in] a Shape function number
  //! \param[in] i Coordinate direction
  const double GetDShape(const int f, const int q, const int a, const int i) const
  { return LocalShapes_[GetFieldIndex(f)]->GetDShapes()
      [q*GetDof(f)*GetNumDerivatives(f)+a*GetNumDerivatives(f)+i]; }
  
  //! Access to Element Geometry
  virtual const ElementGeometry& GetElementGeometry() const = 0;
  
 protected:
  //! Mapping from field to its index in this element
  //! Multiple fields can use the same basis functions.
  //! Therefore, the index need not be consistent with the member Fields_
  //! \param[in] field Global field number
  //! \return Index of given field
  virtual int GetFieldIndex(int field) const = 0;

  //! AddBasisFunctions adds a BasisFunctions pointer at the end of LocalShapes_
  //! The i-th added  BasisFunctions pointer is referenced when GetFieldShapes
  //! returns  the integer i-1
  //! \param[in] field Field number
  //! \param[in] Funcs Basis functions for this field
  inline void AddBasisFunctions(const BasisFunctions &Funcs)
  { LocalShapes_.push_back(Funcs.Clone()); }

  //! Appends a given field to the list of fields in this element
  //! \param[in] field Field number
  inline void AppendField(const int field)
  { Fields_.push_back(field); }
  
 private:
  //! Basis functions for this element indexed by
  //! the local field number
  std::vector<BasisFunctions*> LocalShapes_;

  //! Fields used in this element
  std::vector<int> Fields_;
};

  

#endif

