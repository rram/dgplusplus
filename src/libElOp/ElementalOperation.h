/*
 * ElementalOperation.h
 * DG++
 *
 * Created by Adrian Lew on 10/25/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef ELEMENTALOPERATION
#define ELEMENTALOPERATION

#include "Element.h"

/**
   \brief Computes a residue  on an element

   A Residue object computes the values of a vector function of some or all the 
   fields in the element, with the distinguishing feature that there is one
   component of the function per degree of freedom of the participating fields. 
   
   A Residue object contains two concepts:\n
   1) A procedure to compute the value of the function\n
   2) A pointer to the element over which to compute it\n 

   Each residue acts then as a container of a pointer to an element object, 
   and an operation to perform on that object. In this way the same element can be
   used for several operations in a single computation (The alternative of adding 
   more layers of inheritance to the element classes makes the last possibility very 
   clumsy).
   
   Additionally, operations that need higher-levels of specialization, such as special classes
   of elements, can perform type-checking in their own implementation. 

   The class residue is in fact very similar to a general container,
   in the sense that the object it points to does not need to be an
   element but can be any object that permits the computation of the
   function and can use the (field, dof) notation to label the inputs and 
   outputs.
   
   The precise fields to be utilized in the computation of the
   operation may vary from element type to element type, hence these
   will generally be specified.

   More precisely, a residual is a function 
   \f[
   F^f_a(u^0_0,u^0_1, \ldots, u^0_{n_0-1}, u^0_1, \ldots, u^{K}_{n_{K-1}-1}),
   \f]
   where \f$u^f_a\f$  is the a-th degree of freedom of the f-th participating field in 
   the function. A total of K of the element fields participate as arguments in the 
   function. The f-th participating field has a total of \f$n_f\f$ degrees of freedom. 
   The coefficient of the force "f" runs from 0 to K-1, and "a" ia the degree of freedom 
   index that ranges from 0 to \f$n_f\f$. 

   We need to specify  specify which field in the element will represent the f-th 
   participating field in the function F. For instance, the field number 2 in the element
   can be used as the first argument of F, i.e., as participating field number 0.

   \todo This class does not accept the input of additional parameters that may
   be needed for the evaluation of T that may not be solved for.
   
   \todo The assembly  procedure should probably be changed
*/
class Residue
{
 public:
  Residue() {}
  virtual ~Residue() {}
  Residue(const Residue &NewEl) {}
  virtual Residue * Clone() const = 0;

  //! Returns the fields used for the computation of the residue\n
  //!
  //! GetField()[i] returns the field number beginning from zero.\n
  //! The variable \f$u^f_a\f$ is then computed with field GetField()[f]
  //! in the element.
  virtual const std::vector<int>& GetField() const = 0;


  //! Returns the number of degrees of freedom per field used
  //! for the computation of the residue\n
  //!
  //! GetFieldDof(fieldnum) returns the number of deegrees of freedom
  //! in the participating fieldo number "fieldnumber". The argument 
  //! fieldnumber begins from  zero.\n
  //! The number of different values of "a" in  \f$u^{f}_a\f$ is
  //! then computed with field GetFieldDof(f)
  virtual int  GetFieldDof(int fieldnumber) const = 0;


  //! Returns the value of the residue given the values of the fields.
  //!
  //! @param argval vector of vector<double> containing the values of the degrees of 
  //! freedom for each field.\n
  //! argval[f][a] contains the value of degree of freedom "a" for participating
  //! field "f".
  //!
  //! @param funcval It  returns a vector< vector<double> > with the values of each
  //! component of the residual function. We have that \f$F^f_a\f$=funcval[f][a].
  //! The vector funcval is resized and zeroed in GetVal.
  //! 
  //!
  //! The function returns true if successful, false otherwise.
  virtual void GetVal(const void* argval, 
		      std::vector<std::vector<double>>* funcval) const = 0; 
};






/**
   \brief Computes a residue and its derivative  on an element

   See Residue class for an explanation.

   This class just adds a function GetDVal that contains a vector 
   to return the derivative

*/

class DResidue: public Residue
{
 public: 
  DResidue() {}
  virtual ~DResidue() {}
  inline DResidue(const DResidue &NewEl) {}
  virtual DResidue * Clone() const = 0;
 


  //! Returns the value of the residue and its derivative given the values of the fields.
  //!
  //! @param argval vector of vector<double> containing the values of the degrees of 
  //! freedom for each field.\n
  //! argval[f][a] contains the value of degree of freedom "a" for participating
  //! field "f".
  //!
  //! @param funcval It  returns a vector< vector<double> > with the values of each
  //! component of the residual function. We have that \f$F^f_a\f$=funcval[f][a].
  //! The vector funcval is resized and zeroed in GetVal.
  //! 
  //! @param dfuncval It  returns a vector< vector< vector< vector<double> > > >
  //! with the values of each
  //! component of the derivative of the residual function. 
  //! We have that \f$\frac{\partial F^f_a}{\partial u^g_b}\f$=dfuncval[f][a][g][b].
  //! The vector dfuncval is resized and zeroed in GetVal.
  //!
  //! The function returns true if successful, false otherwise.
  virtual void GetDVal(const void* argval, 
		       std::vector<std::vector<double>> *funcval,
		       std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const = 0; 

  
  //! Consistency test
  //! \param[in] argval Configuration at which to perform the consistency test
  //! \param[in] pertEPS Tolerance to use for perturbation
  //! \param[in] pertEPS Tolerance to use for consistency check
  virtual bool ConsistencyTest(const void* argval,
			       const double pertEPS,
			       const double tolEPS) const = 0;
  
 protected:
  //! Zero the residuals and its derivatives
  void SetZero(std::vector<std::vector<double>>* funcval,
	       std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const;
  
  };


#endif

