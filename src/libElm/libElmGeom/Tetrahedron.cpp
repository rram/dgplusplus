/*
 * Tetrahedron.cpp
 * DG++
 *
 * Created by Ramsharan Rangarajan on 11/07/08.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

// Implementation of class Tetrahedron.

#include "Tetrahedron.h"
#include <cassert>

const std::vector<double> * Tetrahedron::GlobalCoordinatesArray = nullptr;

// X - barycentric coorinates.
// Y - cartesian coorinates.
void Tetrahedron::Map(const double *X, double *Y) const
{
  const int sd = 3;
  
  for(int i=0; i<sd; ++i)
    Y[i] = 
      X[0]*GetCoordinate(0,i) + 
      X[1]*GetCoordinate(1,i) + 
      X[2]*GetCoordinate(3,i) + 
      (1.0-X[0]-X[1]-X[2])*GetCoordinate(2,i);
}


void Tetrahedron::DMap(const double *X, double *DY, double &Jac) const
{
  const int sd = 3; // spatial_dimension.

  for(int i=0; i<sd; ++i) // Loop over x,y,z
    {
      DY[i]      = GetCoordinate(0,i)-GetCoordinate(2,i);
      DY[sd*1+i] = GetCoordinate(1,i)-GetCoordinate(2,i);
      DY[sd*2+i] = GetCoordinate(3,i)-GetCoordinate(2,i);
    }
  
  Jac = 0.;
  for(int i=0; i<sd; ++i)
    {
      int i1 = (i+1)%sd;
      int i2 = (i+2)%sd;
      Jac += DY[i]*(DY[1*sd+i1]*DY[2*sd+i2] - DY[2*sd+i1]*DY[1*sd+i2]);
    }
  
  Jac = std::abs(Jac);
} 


Triangle<3> * Tetrahedron::GetFaceGeometry(const int e) const
{
  int fnodes[] = {2,1,0,
		  2,0,3,
		  2,3,1,
		  0,1,3};
  assert( (e>=0 && e<=3) && "Tetrahedron::GetFaceGeometry() : Request for invalid face. Quitting...\n" );
  const auto& conn = GetConnectivity();
  return new Triangle<3>(conn[fnodes[3*e+0]], 
			 conn[fnodes[3*e+1]], 
			 conn[fnodes[3*e+2]]);
} 
