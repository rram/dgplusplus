// Sriramajayam

/*
 * testPlotVTK.cpp
 * DG++
 *
 * Created by Ramsharan Rangarajan on 08/20/2010.
 *  
 * Copyright (c) 2006 Adrian Lew.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "PlottingUtils.h"
int main()
{
  // Read a simple mesh
  CoordConn MD;
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  FemapCall((char*)"/home/rram/DG++/Meshes/2D/squareCoarse.NEU", 
	    MD.coordinates, MD.connectivity, MD.spatial_dimension, 
	    MD.nodes_element, MD.nodes, MD.elements);
  
  // Create two scalar field over the nodes
  std::vector< std::vector<double> > nScalars(2);
  nScalars[0].clear();
  nScalars[1].clear();
  for(int n=0; n<MD.nodes; n++)
    {
      nScalars[0].push_back( MD.coordinates[2*n]+MD.coordinates[2*n+1] );
      nScalars[1].push_back( MD.coordinates[2*n]-MD.coordinates[2*n+1] );
    }
  
  // Create a vector field over the nodes
  std::vector< std::vector<double> > nVectors(1);
  nVectors[0].clear();
  for(int n=0; n<MD.nodes; n++)
    {
      nVectors[0].push_back( 0. );
      nVectors[0].push_back( 1. );
      nVectors[0].push_back( 1. );
    }
  
  // Create an elemental scalar field
  std::vector< std::vector<double> > eScalars(1);
  eScalars[0].clear();
  for(int e=0; e<MD.elements; e++)
    eScalars[0].push_back( double(e) );
  
  // Create an elemental vector field
  std::vector< std::vector<double> > eVectors(1);
  eVectors[0].clear();
  for(int e=0; e<MD.elements; e++)
    {
      eVectors[0].push_back( double( e%5 ) );
      eVectors[0].push_back( double( e%7) );
      eVectors[0].push_back( double( e%11) );
    }
  
  // Plot this mesh in VTK format
  PlotVTKCoordConnWithNodalAndElementalFields((char*)"square.vtp", MD,
					      &nScalars, &nVectors,
					      &eScalars, &eVectors); 
}
