// Sriramajayam

#include <ElementalOperation.h>

// Zero the residuals and derivatives
void DResidue::
SetZero(std::vector<std::vector<double>>* funcval,
	std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
{
  const int nFields = static_cast<int>(GetField().size());
  std::vector<int> nDofs(nFields);
  for(int f=0; f<nFields; ++f)
    nDofs[f] = GetFieldDof(f);
      
      
  if(funcval!=nullptr)
    {
      for(int f=0; f<nFields; ++f)
	for(int a=0; a<nDofs[f]; ++a)
	  (*funcval)[f][a] = 0.;
    }
  if(dfuncval!=nullptr)
    {
      for(int f=0; f<nFields; ++f)
	for(int a=0; a<nDofs[f]; ++a)
	  for(int g=0; g<nFields; ++g)
	    for(int b=0; b<nDofs[g]; ++b)
	      (*dfuncval)[f][a][g][b] = 0.;
    }
  return;
}

   
