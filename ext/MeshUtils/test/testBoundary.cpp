// Sriramajayam

/*
 * testBoundary.cpp
 * DG++
 *
 * Created by Ramsharan Rangarajan on 05/04/2011.
 *  
 * Copyright (c) 2006 Adrian Lew.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "MeshUtils.h"
#include "PlottingUtils.h"

int main()
{
  // Read mesh of an annulus
  char filename[1000];
  sprintf(filename, "%s/Meshes/2D/Ring.NEU", std::getenv("DGPLUS_HOME"));
  CoordConn MD;
  MD.spatial_dimension = 2;
  MD.nodes_element = 3;
  FemapCall(filename, MD.coordinates, MD.connectivity, MD.spatial_dimension,
	    MD.nodes_element, MD.nodes, MD.elements);
  
  // Get the list of boundary faces
  std::vector< std::vector<int> > Boundary = 
    GetBoundaryFacesInTriangleMesh(MD.connectivity);
  std::cout<<"\nNumber of connected components in boundary = "<<Boundary.size()
	   <<" Should be 2.\n";
  
  // Plot free edges a triangles
  std::vector< std::vector<int> > Conn(Boundary.size());
  for(unsigned int i=0; i<Boundary.size(); i++)
    {
      Conn[i].clear();
      for(unsigned int e=0; e<Boundary[i].size()/2; e++)
	{
	  Conn[i].push_back( Boundary[i][2*e] );
	  Conn[i].push_back( Boundary[i][2*e+1] );
	  Conn[i].push_back( Boundary[i][2*e+1] );
	}
    }
  CoordConn ED = MD;
  ED.connectivity = Conn[0];
  ED.elements = ED.connectivity.size()/3;
  PlotTecCoordConn((char*)"Bd1.tec", ED);
  ED.connectivity = Conn[1];
  ED.elements = ED.connectivity.size()/3;
  PlotTecCoordConn((char*)"Bd2.tec", ED);
    
  // Get the list of boundary nodes
  std::vector< std::vector<int> > BdNodes = 
    GetBoundaryNodesInTriangleMesh(MD.connectivity);
  for(unsigned int i=0; i<BdNodes.size(); i++)
    {
      std::cout<<"\n\nNodes in component "<<i+1<<": \n";
      for(unsigned int j=0; j<BdNodes[i].size(); j++)
	std::cout<<BdNodes[i][j]<<", ";
    }
  std::cout<<"\n\n";
}
