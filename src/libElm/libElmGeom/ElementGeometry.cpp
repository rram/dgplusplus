/*
 * ElementGeometry.cpp
 * DG++
 *
 * Created by Adrian Lew on 9/4/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "ElementGeometry.h"
#include <cmath>
#include <cassert>

bool ElementGeometry::ConsistencyTest(const double * X, const double Pert) const
{
  assert( Pert>0. && 
	  "\nElementGeometry::ConsistencyTest - Pert cannot be less or equal than zero\n");
  
  const int pDim = GetParametricDimension();
  const int eDim = GetEmbeddingDimension();
  
  double *DYNum = new double[pDim*eDim];
  double *DY    = new double[pDim*eDim];
  double *Xpert = new double[pDim];
  double *Yplus = new double[eDim];
  double *Yminus = new double[eDim];
  double Jac;

  for(int a=0; a<pDim; a++) Xpert[a]=X[a];

  DMap(X,DY,Jac);

  for(int a=0; a<pDim; ++a)
    {
      Xpert[a] = X[a] + Pert;
      Map(Xpert,Yplus);

      Xpert[a] = X[a] - Pert;
      Map(Xpert,Yminus);

      Xpert[a] = X[a];

      for(int i=0; i<eDim; ++i)
	DYNum[a*eDim+i] = (Yplus[i]-Yminus[i])/(2*Pert);
    }
  
  double error = 0;
  double normX = 0;
  double normDYNum = 0;
  double normDY = 0;

  for(int a=0; a<pDim; ++a)
    {
      normX += X[a]*X[a];
      
      for(int i=0; i<eDim; ++i)
	{
	  error += pow(DY[a*eDim+i]-DYNum[a*eDim+i],2.);
	  normDY += pow(DY[a*eDim+i],2.);
	  normDYNum += pow(DYNum[a*eDim+i],2.);
	}
    }
  error = sqrt(error);
  normX = sqrt(normX);
  normDY = sqrt(normDY);
  normDYNum = sqrt(normDYNum);

  delete[] Yplus;
  delete[] Yminus;
  delete[] Xpert;
  delete[] DYNum;
  delete[] DY;

  if(error*(normX+Pert) < (normDY<normDYNum? normDYNum: normDY)*Pert*10)
    return true;
  else
    return false;
}


