/*
 * testStressWork.cpp
 * DG++
 *
 * Created by Adrian Lew on 10/25/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "StressWork.h"
#include "P12DElement.h"
#include <iostream>
#include <cstdlib>
#include <cassert>
#include <random>


int main()
{
  constexpr int SPD = 2;

  // Reference configuration
  std::vector<double> Vertices({1,0,0,1,0,0,1,1});
  Triangle<SPD>::SetGlobalCoordinatesArray(Vertices);

  // Create 1 element
  std::vector<Element*> ElmArray(1);
  ElmArray[0] = new P12DElement<SPD>(1,2,3);
  StandardP12DMap L2GMap(ElmArray);
  
  // Create displacement map
  VectorMap dispmap(Vertices.size()/SPD, SPD);

  // Random nodal displacement
  std::random_device rd;  //Will be used to obtain a seed for the random number engine
  std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
  std::uniform_real_distribution<> dis(-0.2,0.2);
  for(int a=0; a<3; ++a)
    {
      double ndisp[] = {dis(gen), dis(gen)};
      dispmap.Set(a, ndisp);
    }
  dispmap.SetInitialized();
  
  // Create operation
  NeoHookean NH(1.,1.);
  Workspace WrkSpc(SPD);
  const std::vector<int> Fields({0,1});
  VectorMapAccess VMAccess(std::vector<int>({0,1,2})); // Access global displacements
  StressWork Op(ElmArray[0], NH, Fields, VMAccess, WrkSpc);

  // Test operation
  assert(static_cast<int>(Op.GetField().size())==SPD);
  assert(Op.GetField()[0]==0 && Op.GetField()[1]==1);
  for(int f=0; f<2; ++f)
    assert(Op.GetFieldDof(f)==3);
  assert(Op.GetElement()==ElmArray[0]);
  assert(&Op.GetSimpleMaterial()==&NH);
  
  // Consistency test
  const double pertEPS = 1.e-5;
  const double tolEPS = 1.e-4;
  bool flag = Op.ConsistencyTest(&dispmap, pertEPS, tolEPS);
  assert(flag && "DResidue::Consistency test failed");

  // Clean up
  delete ElmArray[0];
}
