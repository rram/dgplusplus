/*
 * Triangle.h
 * DG++
 *
 * Created by Adrian Lew on 9/4/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#ifndef TRIANGLE
#define TRIANGLE

#include "Segment.h"
#include <cmath>


/**
   \brief Triangle: Geometry of planar triangles 
   
   A Triangle is:\n
   1) A set of indices that describe the connectivity of the triangle, 
   properly oriented. The coordinates
   are not stored in the element but wherever the application decides\n
   2) An affine map from a two-dimensional triangle (parametric configuration) 
   with area 1/2 to the convex  
   hull of the three vertices. Triangles embedded in three-dimensional space 
   are hence easily handled. \n
   
   The parametric configuration is the triangle (0,0),(1,0),(0,1).\n
   The two parametric coordinates used are the ones aligned with the two axes in 2D.

   For a triangle with connectivity (1,2,3) the faces are ordered as
   (1,2),(2,3),(3,1).

   Rationale for a templated class: prevent having multiple copies of
   the dimension of spatial_dimension in each one of the multiple
   elements in a mesh. A static variable for it would have only
   allowed to use one type of triangles in a program.

   \warning Neither Map nor DMap check for bounds of
   their array arguments
*/


template<int spatial_dimension> 
class Triangle: public ElementGeometry
{
 public:
  //! Connectivity in Triangle<spatial_dimension> GlobalCoordinatesArray
  Triangle(const int i1, const int i2, const int i3)
    {
      Connectivity.push_back(i1);
      Connectivity.push_back(i2);
      Connectivity.push_back(i3);
    }
  
  inline virtual ~Triangle(){}

  inline Triangle(const Triangle& T)
  { Connectivity = T.Connectivity; }

  inline virtual Triangle<spatial_dimension>* Clone() const override
  { return new Triangle<spatial_dimension>(*this); }
  
  inline int GetNumberOfVertices() const override
  { return 3; }

  inline const std::vector<int>& GetConnectivity() const override
  { return Connectivity; }

  inline std::string GetPolytopeName() const override
  { return "TRIANGLE"; }

  inline int GetParametricDimension() const override
  { return 2; }

  inline int GetEmbeddingDimension() const override
  { return spatial_dimension; }

  void Map(const double * X, double *Y) const override; 

  void DMap(const double * X, double *DY, double &Jac) const override; 

  inline int GetNumberOfFaces() const override
  { return 3; }

  Segment<spatial_dimension>* GetFaceGeometry(const int e) const override; 

  //! Set the only static member of the class GlobalCoordinateArray
  //! @param coord pointer to the array of all vertex coordinates
  inline static void SetGlobalCoordinatesArray(const std::vector<double>& coord) 
  { GlobalCoordinatesArray = &coord; }

 protected:
  //! Interface to avoid explicitly using GlobalCoordinatesArray everywhere, 
  //! since the latter is likely 
  //! to change later on
  //! @param a local node number
  //! @param i coordinate number
  virtual double  GetCoordinate(int a,int i) const
  { return (*GlobalCoordinatesArray)
      [(GetConnectivity()[a]-1)*spatial_dimension+i]; }
  
 private:
  static const std::vector<double>* GlobalCoordinatesArray;  //<! Pointer to the global coordinates array
  std::vector<int> Connectivity;
};



// Class implementation

template<int spatial_dimension> 
const std::vector<double> * Triangle<spatial_dimension>::GlobalCoordinatesArray = nullptr;


template<int spatial_dimension> 
void Triangle<spatial_dimension>::Map(const double * X, double *Y) const
{
  for(int i=0; i<spatial_dimension; ++i)
    Y[i] = X[0]*GetCoordinate(0,i) + X[1]*GetCoordinate(1,i) + (1-X[0]-X[1])*GetCoordinate(2,i);

  return;
}


template<int spatial_dimension> 
void Triangle<spatial_dimension>::DMap(const double* X, double* DY, double& Jac) const
{
  for(int i=0; i<spatial_dimension; ++i)
    {
      DY[                  i] = GetCoordinate(0,i) - GetCoordinate(2,i);
      DY[spatial_dimension+i] = GetCoordinate(1,i) - GetCoordinate(2,i);
    }
  
  double g11=0.;
  double g22=0.;
  double g12=0.;

  for(int i=0; i<spatial_dimension; ++i)
    {
      g11 += DY[i]*DY[i];
      g22 += DY[spatial_dimension+i]*DY[spatial_dimension+i];
      g12 += DY[spatial_dimension+i]*DY[i];
    }
  
  Jac=sqrt(g11*g22-g12*g12);
  
  return;
}


template<int spatial_dimension>
Segment<spatial_dimension>* 
Triangle<spatial_dimension>::GetFaceGeometry(const int e) const
{
  switch(e)
    {
    case 0:
      return new Segment<spatial_dimension>( GetConnectivity()[0], 
					     GetConnectivity()[1] );
      break;

    case 1:
      return new Segment<spatial_dimension>( GetConnectivity()[1], 
					     GetConnectivity()[2] );
      break;

    case 2:
      return new Segment<spatial_dimension>( GetConnectivity()[2], 
					     GetConnectivity()[0] );
      break;
	  
    default:
      return 0;
    }
}

#endif
