/*
 * testmeshP12D.cpp
 * DG++
 *
 * Created by Adrian Lew on 10/25/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 
 
#include "P12DElement.h"
#include "StressWork.h"
#include "Assembler.h"
#include "MeshUtils.h"
#include "PlottingUtils.h"
#include "PetscData.h"
#include <set>
#include <cstdio>

// Dirichlet BCs:
void GetDirichletBCs(const std::vector<Element *> &EA,
		     const LocalToGlobalMap & L2GMap,
		     std::vector<int>& boundary,
		     std::vector<double>& bvalues);

int main(int argc, char **argv)
{
  PetscInitialize(&argc,&argv,PETSC_NULL,PETSC_NULL);
  constexpr int SPD = 2;

  // read the mesh over a square
  CoordConn CC;
  CC.nodes_element = 3;
  CC.spatial_dimension = SPD;
  ReadTecplotFile((char*)"square.msh", CC);
  // Shift origin and scale to size 10.
  for(int n=0; n<CC.nodes; ++n)
    for(int k=0; k<2; ++k)
      {
	CC.coordinates[2*n+k] += 0.5;
	CC.coordinates[2*n+k] *= 10.;
      }
  PlotTecCoordConn((char*)"refConfig.tec", CC);
  
  // Subdivide triangles if specified.
  int ndiv = 0;
  PetscOptionsGetInt(PETSC_NULL,"-ndiv", &ndiv, PETSC_NULL);
  for(int i=0; i<ndiv; ++i)
    SubdivideTriangles(CC.connectivity, CC.coordinates, CC.nodes, CC.elements);
  
  // Indicate where the global coordinates array is:
  Triangle<2>::SetGlobalCoordinatesArray(CC.coordinates);
  Segment<2>::SetGlobalCoordinatesArray(CC.coordinates);

  // Setup elements
  std::vector<Element*> ElementArray(CC.elements);
  for(int e=0; e<CC.elements; ++e)
    ElementArray[e] = new P12DElement<SPD>(CC.connectivity[3*e],CC.connectivity[3*e+1],CC.connectivity[3*e+2]);

  // Local to global map
  StandardP12DMap L2GMap(ElementArray);

  // Displacement field, initialize all displacements to zero
  VectorMap DispMap(CC.nodes, SPD);
  double zero[SPD] = {0.,0.};
  for(int a=0; a<CC.nodes; ++a)
    DispMap.Set(a, zero);
  DispMap.SetInitialized();

  // Create operations
  std::vector<StressWork*> OperationsArray(CC.elements);
  IsotropicLinearElastic ILE(1.0,1.0);       // Linear material
  Workspace WrkSpc(SPD);
  const std::vector<int> Fields({0,1});
  for(int e=0; e<CC.elements; e++)
    {
      VectorMapAccess VMAccess(std::vector<int>({CC.connectivity[3*e]-1,
	      CC.connectivity[3*e+1]-1,CC.connectivity[3*e+2]-1}));
      OperationsArray[e] = new StressWork(ElementArray[e], ILE, Fields, VMAccess, WrkSpc);
    }
  
  // Assembler
  StandardAssembler<StressWork> Asm(OperationsArray, L2GMap);
  std::vector<int> nz;
  Asm.CountNonzeros(nz);
  
  // Initialize petsc data
  PetscErrorCode ierr;
  PetscData PD;
  PD.Initialize(nz);
  
  // Set up boundary conditions (assumes no repetitions)
  std::vector<double> bvalues;
  std::vector<int> boundary;
  GetDirichletBCs(ElementArray, L2GMap, boundary, bvalues);
  
  // Set prescribed displacement values
  const int nbcs = static_cast<int>(bvalues.size());
  std::set<int> bcset;
  for(int i=0; i<nbcs; ++i)
    if(bcset.find(boundary[i])==bcset.end()) // Don't increment the displacement twice
      {
	int nodenum = boundary[i]/SPD;
	int dir = boundary[i]%SPD;
	double val[SPD] = {0.,0.};
	val[dir] = bvalues[i];
	DispMap.Increment(nodenum, val);
	bcset.insert(boundary[i]);
      }
  DispMap.SetInitialized();
  
  // Values to be set for increments in displacements
  std::fill(bvalues.begin(), bvalues.end(), 0.);
  
  // Start Newton Raphson:
  VecZeroEntries(PD.solutionVEC);
  while(true)
    {
      // Update the displacement map
      double* deltaDisp;
      ierr = VecScale(PD.solutionVEC, -1.); CHKERRQ(ierr);
      ierr = VecGetArray(PD.solutionVEC, &deltaDisp); CHKERRQ(ierr);
      for(int a=0; a<CC.nodes; ++a)
	DispMap.Increment(a, &deltaDisp[SPD*a]);
      DispMap.SetInitialized();
      ierr = VecRestoreArray(PD.solutionVEC, &deltaDisp); CHKERRQ(ierr);

      // Next update
      Asm.Assemble(&DispMap, PD.resVEC, PD.stiffnessMAT);
      PD.SetDirichletBCs(boundary, bvalues);
      PD.Solve();
      if(PD.HasConverged(1.e-10,1,1.0))
	{	  
	  // Plot
	  CoordConn DefMD = CC;
	  const auto& disp = DispMap.Get();
	  for(int i=0; i<CC.nodes*SPD; ++i)
	    DefMD.coordinates[i] += disp[i];
	  PlotTecCoordConn((char*)"defConfig.tec", DefMD);
	  break;
	}
      std::fflush( stdout );
    }
  // end Newton-Raphson

  // Clean up
  for(int e=0; e<CC.elements; ++e)
    {
      delete ElementArray[e];
      delete OperationsArray[e];
    }
  PD.Destroy();
  PetscFinalize();
}



// Value of Dirichlet boundary conditions:
bool BCValue(double *X, double * bvals)
{
  double eps=1e-3;
  // Fix bottom. Move top.
  if(std::abs(X[1]-0.)<=eps || std::abs(X[1]-10.)<=eps || std::abs(X[0]-0.)<=eps || std::abs(X[0]-10.)<=eps)
    {
      bvals[0] = 5.0*X[1]*X[1]/(10*10);
      bvals[1] = 0.;
      return true;
    }
  else
    return false;
}


// Dirichlet boundary conditions:
void GetDirichletBCs(const std::vector<Element*> &EA, const LocalToGlobalMap &L2GMap,
		     std::vector<int>& boundary, std::vector<double>& bvalues)
{
  double param[] = {1,0,
		    0,1,
		    0,0};
  
  for(unsigned int e=0; e<EA.size(); e++)
    {
      double Coord[2];
      double bvals[2];
      
      for(int a=0; a<3; a++)
	{
	  EA[e]->GetElementGeometry().Map(param+2*a, Coord);
	  if( BCValue(Coord, bvals) )
	    for(int f=0; f<2; f++)
	      {
		boundary.push_back( L2GMap.Map(f, a, e) );
		bvalues.push_back( bvals[f] );
	      }
	}
    }
}
