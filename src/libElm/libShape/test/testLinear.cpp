/*
 * testLinear.cpp
 * DG++
 *
 * Created by Adrian Lew on 9/9/06.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "Linear.h"
#include <iostream>
#include <cassert>
#include <cmath>

int main()
{
  // 2D test
  double coord2[] = {0.1,0.8};

  Linear<2> Linear2D;
  assert(Linear2D.GetNumberOfFunctions()==3);
  assert(Linear2D.GetNumberOfVariables()==2);
  assert(Linear2D.ConsistencyTest(coord2,1.e-6));

  // test copy
  Linear<2> Linear2DCopy(Linear2D);
  for(int a=0; a<Linear2D.GetNumberOfFunctions(); a++)
    assert(std::abs(Linear2DCopy.Val(a,coord2)-Linear2D.Val(a,coord2))<1.e-8);

  // 3D test 
  double coord3[] = {1.2, 0.1, -0.4};
  Linear<3> Linear3D;
  assert(Linear3D.GetNumberOfFunctions()==4);
  assert(Linear3D.GetNumberOfVariables()==3);
  assert(Linear3D.ConsistencyTest(coord3,1.e-6));
}
