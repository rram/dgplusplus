/*
 * BodyForceWork.cpp
 * DG++
 *
 * Created by Adrian Lew on 09/13/07.
 *  
 * Copyright (c) 2006 Adrian Lew
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including 
 * without limitation the rights to use, copy, modify, merge, publish, 
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject
 * to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included 
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */ 

#include "BodyForceWork.h"
#include <cmath>

void BodyForceWork::GetDVal(const void* config, 
			    std::vector<std::vector<double>>* funcval,
			    std::vector<std::vector<std::vector<std::vector<double>>>>* dfuncval) const
{
  // Initialize outputs to zero
  SetZero(funcval, dfuncval);
  
  // Get the deformation mapping
  auto* dispptr = static_cast<const VectorMap*>(config);
  assert(dispptr!=nullptr && "BodyForceWork::GetDVal- Displacement map is invalid");
  auto& DispMap = *dispptr;
  assert(DispMap.IsInitialized() && "BodyForceWork::GetDVal- Displacement map not initialized"); 

  // Local node numbers
  const auto& LocNodes = VMAccess.Get();
  
  const int Dim = static_cast<int>(FieldsUsed.size());
  
  // We should have the same quadrature points in  all fields used
  unsigned int nquad = Elm->GetIntegrationWeights(FieldsUsed[0]).size(); 
  std::vector<int> nDof;
  std::vector< std::vector< double > > IntWeights(Dim);
  std::vector< std::vector< double > > Shape(Dim);
  
  for(int f=0; f<Dim; ++f)
    {
      nDof.push_back(Elm->GetDof(FieldsUsed[f]));
      IntWeights[f] = Elm->GetIntegrationWeights(FieldsUsed[f]);
      Shape[f] = Elm->GetShape(FieldsUsed[f]);
    }
  
  int spd = Elm->GetElementGeometry().GetEmbeddingDimension();
  std::vector<double> uVal(Dim);
  std::vector<double> XVal(spd);
  std::vector<double> ForceVal(Dim);
  std::vector<double> DForceVal(Dim*Dim);
  bool flag;
  for(unsigned int q=0; q<nquad; ++q)
    {
      for(int f=0; f<Dim; ++f)
	{
	  uVal[f] = 0;
	  for(int a=0; a<nDof[f]; ++a)
	    {
	      const auto* NodalDisp = DispMap.Get(LocNodes[a]);
	      uVal[f] += NodalDisp[f]*Shape[f][q*nDof[f]+a];
	    }
	}
      
      for(int f=0; f<spd; ++f)
	XVal[f] = Elm->GetIntegrationPointCoordinates(0)[q*spd+f];
      
      if(dfuncval!=nullptr)
	{
	  flag = LocalForceField->GetDForceField( uVal, XVal, &ForceVal, &DForceVal);
	  assert(flag && "\nBodyForceWork.cpp: Error in ForceField\n");
	}
      else
	{
	  flag = LocalForceField->GetDForceField( uVal, XVal, &ForceVal, 0);
	  assert(flag && "\nBodyForceWork.cpp: Error in the ForceField\n");
	}
      
      
      for(int f=0; f<Dim; ++f)
	for(int a=0; a<nDof[f]; ++a)
	  (*funcval)[f][a] +=  IntWeights[f][q] * ForceVal[f] * Shape[f][q*nDof[f]+a];
      
      if(dfuncval!=0)
	for(int f=0; f<Dim; ++f)
	  for(int a=0; a<nDof[f]; ++a)
	    for(int g=0; g<Dim; ++g)
	      for(int b=0; b<nDof[g]; ++b)
		(*dfuncval)[f][a][g][b] += IntWeights[f][q] * DForceVal[f*Dim+g] 
		  * Shape[f][q*nDof[f]+a] * Shape[g][q*nDof[g]+b];
    }
  return;
}
